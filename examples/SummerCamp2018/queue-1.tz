tikz [line width=1pt]
{
	\usepackage{mathpazo}

	\definecolor{mygreen}{HTML}{1B5E20}
	\definecolor{myred}{HTML}{d62728}

	var H = 1.1, h1 = 0.75, h2 = 0.35;
	var W = 3.6, w1 = 0.1, w2 = 3.5;
	var T = 5.2, D = 2.2;

	// push
	goto (0, 0)
	{
		draw rect (0, 0) to (W, H);

		draw [right] text "rear" at (w1, h1);
		draw [right] text "front" at (w1, h2);

		draw [left] text "5 :: 2 :: []" at (w2, h1);
		draw [left] text "[]" at (w2, h2);
	}

	goto (T, 0)
	{
		draw rect (0, 0) to (W, H);

		draw [right] text "rear" at (w1, h1);
		draw [right] text "front" at (w1, h2);

		draw [left] text "{\color{mygreen} 3 ::} 5 :: 2 :: []" at (w2, h1);
		draw [left] text "[]" at (w2, h2);

		draw [right,font=\small] text "actual time: 1" at (0.8, -0.22);
	}

	draw [font=\small] text "push(3)" at ((T+W)/2, H*0.55);
	draw text "$\longrightarrow$" at ((T+W)/2, H*0.3);

	// pop-1
	goto (T, -D)
	{
		draw rect (0, 0) to (W, H);

		draw [right] text "rear" at (w1, h1);
		draw [right] text "front" at (w1, h2);

		draw [left] text "[]" at (w2, h1);
		draw [left] text "{\color{myred} 2 ::} 5 :: 3 :: []" at (w2, h2);

		draw [right,font=\small] text "actual time: k+1" at (0.8, -0.22);

		draw [<-] arc (W+0.1, H/2) radius (h1-h2)/2 range -90:90;
		draw [right,font=\small] text "rev" at (W+0.1+(h1-h2)/2, H/2)

		var s = (0.5, (H+D)/2);
		goto s rotate 270 draw text "$\longrightarrow$" at (0, 0);
		draw [left,font=\small] text "pop" at s;
	}

	// pop-2
	goto (T, -D*2)
	{
		draw rect (0, 0) to (W, H);

		draw [right] text "rear" at (w1, h1);
		draw [right] text "front" at (w1, h2);

		draw [left] text "[]" at (w2, h1);
		draw [left] text "{\color{myred} 5 ::} 3 :: []" at (w2, h2);

		draw [right,font=\small] text "actual time: 1" at (0.8, -0.22);

		var s = (0.5, (H+D)/2);
		goto s rotate 270 draw text "$\longrightarrow$" at (0, 0);
		draw [left,font=\small] text "pop" at s;
	}


	// text
	draw text [align=center] multiline {
		"There are both \textit{expensive}",
		"and \textit{cheap} pop operations"
	} at (W/2+0.25, H/2-D);
}