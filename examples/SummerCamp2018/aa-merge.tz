tikz [line width=1pt]
{
	\usepackage{mathpazo}
	\usepackage{amssymb}

	var r = 0.24;

	def vertex(pair v, string s) {
		draw circle v radius r;
		draw [font=\scriptsize] text s at v
	}

	def tree(pair v, string s) {
		var p1 = v + (-0.3, -0.6);
		var p2 = v + ( 0.3, -0.6);
		var d = r / dis(v, p1);
		var p0 = v * (1-d) + p1 * d;
		var p3 = v * (1-d) + p2 * d;
		vertex(v, "");
		draw [font=\scriptsize] text s at v+(0, -0.4);
		draw [rounded corners=1pt] line p0 -- p1 -- p2 -- p3;
	}

	def link(pair v1, pair v2) {
		var d = r / dis(v1, v2);
		var p = v1 * (1-d) + v2 * d;
		var q = v1 * d + v2 * (1-d);
		draw line p -- q;
	}

	def db_link(pair v1, pair v2) {
		var d = r / dis(v1, v2);
		var p = v1 * (1-d) + v2 * d;
		var q = v1 * d + v2 * (1-d);
		draw [<->] line p -- q;
	}

	def dash_link(pair v1, pair v2) {
		var d = r*1.2 / dis(v1, v2);
		var p = v1 * (1-d) + v2 * d;
		var q = v1 * d + v2 * (1-d);
		draw [<->,densely dashed] line p -- q;
	}

	var h = 0.7
	var x = (0, 0), y = (-0.9, -h);
	var a = (-1.8, -h*2), b = (-0, -h*2)

	goto(0, 0)
	{
		vertex(x, "x")
		tree(y, "A")
		link(x, y)		
	}

	goto(2, 0)
	{
		vertex(x, "y")
		tree(y, "B")
		link(x, y)		
	}

	draw text "$\leadsto$" at (2.8, -h)

	goto(5.5, 0.2)
	{
		vertex(x, "x");
		vertex(y, "y");
		tree(a, "B");
		tree(b, "A");
		db_link(x, y)
		dash_link(a, b)
		link(a, y)
		link(b, y)
	}

	// draw text "or" at (6.3, -h)

	comment goto(8.9, 0.2)
	{
		vertex(x, "y");
		vertex(y, "x");
		tree(a, "A");
		tree(b, "B");
		link(x, y)
		link(a, y)
		link(b, y)
	}
}