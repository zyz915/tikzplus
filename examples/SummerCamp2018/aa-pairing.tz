tikz [line width=1pt]
{
	\usepackage{mathpazo}
	\usepackage{amssymb}

	var r = 0.24;

	def vertex(pair v, string s) {
		draw circle v radius r;
		draw [font=\scriptsize,right] text s at v+(r*0.7,r*0.8)
	}

	def tree(pair v, string n, string s) {
		var p1 = v + (-0.3, -0.6);
		var p2 = v + ( 0.3, -0.6);
		var d = r / dis(v, p1);
		var p0 = v * (1-d) + p1 * d;
		var p3 = v * (1-d) + p2 * d;
		vertex(v, n);
		//draw [font=\scriptsize] text s at v+(0, -0.4);
		draw [rounded corners=1pt] line p0 -- p1 -- p2 -- p3;
	}

	def link(pair v1, pair v2) {
		var d = r / dis(v1, v2);
		var p = v1 * (1-d) + v2 * d;
		var q = v1 * d + v2 * (1-d);
		draw line p -- q;
	}

	def dashed(pair v1, pair v2) {
		var d = r / dis(v1, v2);
		var p = v1 * (1-d) + v2 * d;
		var q = v1 * d + v2 * (1-d);
		draw [densely dashed] line p -- q;
	}

	var h = 0.6, hh = 0.3
	var r0 = (0, 0), r1 = (-0.8, -h), dr = (0.6, 0.8);
	var d = (0.8, -hh)

	draw [font=\small,right] text "deleted root" at dr + (r, 0);

	vertex(dr, "");
	dashed(r0, dr)
	vertex(r0, "$x_0$");
	tree(r1, "", "$A_0$");
	link(r1, r0);

	vertex(r0+d*1, "$x_1$");
	tree(r1+d*1, "", "$A_1$");
	link(r0+d*1, r0);
	link(r1+d*1, r0+d*1)

	vertex(r0+d*2.5, "$x_{2k-1}$");
	tree(r1+d*2.5, "", "$A_{2k-1}$");
	dashed(r0+d*2.5, r0+d*1);
	link(r1+d*2.5, r0+d*2.5);

	vertex(r0+d*3.5, "$x_{2k}$");
	tree(r1+d*3.5, "", "$A_{2k}$");
	link(r0+d*3.5, r0+d*2.5);
	link(r1+d*3.5, r0+d*3.5)
}