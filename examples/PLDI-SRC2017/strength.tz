tikz [line width=0.9pt,font=\scriptsize]
{
  \usepackage{amssymb}
  \usepackage{mathpazo}
  \usetikzlibrary{snakes}
  \usepackage{pifont}
  \newcommand{\cmark}{\ding{51}}
  \newcommand{\xmark}{\ding{55}}
  \definecolor{xred}{RGB}{200,82,0}

  var H = 0.75, W = 0.75;
  var r = (0, 0);
  var p = (W*0.63, 0.16*H), q = (-W*0.63, 0.16*H);
  var up= (W*0.37,  0.5*H), uq =(-W*0.35, 0.53*H);
  var v10 = (-W, -0.8*H);
  var v01 = ( W,  0.8*H);
  var v20 = (-2*W, -1.6*H);
  var v11 = ( W, -0.8*H);
  var R = 0.18;
  var X = 1.5 * W;
  var pp = r * 0.6 + v01 * 0.4;

  def del(pair v1, pair v2) {
    var c = v1 * 0.55 + v2 * 0.45;
    var deg = atan2(snd(v2-v1), fst(v2-v1));
    var dx = 0.04, dy = 0.07;
    goto c rotate deg with [line width=1pt,color=white] {
	  draw line (dx, dy) -- -(dx, dy);
	  draw line (dx,-dy) -- -(dx,-dy);
	}
    goto c rotate deg with [line width=0.7pt,color=xred] {
	  draw line (dx, dy) -- -(dx, dy);
	  draw line (dx,-dy) -- -(dx,-dy);
	}
  }

  def edge(pair v1, pair v2) {
    var d = dis(v1, v2);
    var p = v2 - v1;
    var u = p / d * 0.98;
    draw [->] line (v1+u*R) -- (v1+u*(d-R))
  };

  def dash(pair v1, pair v2) {
    var d = dis(v1, v2);
    var p = v2 - v1;
    var u = p / d * 0.98;
    draw [->,densely dashed] line (v1+u*R) -- (v1+u*(d-R))
  };

  goto (0, 0) {
    draw circle r   radius R;
    draw circle v10 radius R;

    draw [->] arc r+(0,1.45*R)
      radius 0.9*R range -50 : 230;

    edge(v10, r);

    draw text "$u$" at v10;
    draw text "\texttt{P}" at v10+p;
    draw text "\texttt{P}" at pp;
    draw [font=\scriptsize] 
      text "\texttt{P[P[u]] == P[u]}" at r-(0,1.35*H);

    draw [font=\scriptsize\it,align=center]
      text "pattern\\[-1pt]matching" at r-(1.45*W,0);
  }

  goto (0, 2.4*H) {
    draw circle r   radius R;
    draw circle v10 radius R;
    draw circle v11 radius R;

    edge(r, v10);
    edge(r, v11);

    draw text "$u$" at r;
    draw [font=\scriptsize\tt] text "'a'" at v10-(0.55*W, 0);
    draw [font=\scriptsize\tt] text "'b'" at v11+(0.55*W, 0);
   // draw text "$b$" at v11;
    draw text "\texttt{P}" at v10+p;
    draw text "\texttt{Q}" at v11+q;
    draw [font=\scriptsize\tt,align=left] 
      text "Label[P[u]] == 'a' \&\&\\[-1pt]Label[Q[u]] == 'b'"
      at r-(0.2*W, 1.45*H);

    draw [font=\scriptsize\it,align=center]
      text "pattern\\[-1pt]matching" at r-(1.45*W,0);
  }

  goto (2*X, 0) {
    draw circle r   radius R;
    draw circle v10 radius R;
    draw circle v11 radius R;

    draw [->] arc r+(0,1.45*R)
      radius 0.9*R range -50 : 230;

    edge(v10, r);
    dash(r, v11);
    edge(v10, v11);

    draw text "$u$" at v10;
    draw text "\texttt{P}" at pp;
    draw text "\texttt{P}" at v10+up;
    draw text "\texttt{P}" at v11+uq;
    draw text "\texttt{Q}" at r-(0,1.05*H);
    draw [font=\scriptsize] 
      text "\texttt{P[P[u]] <?= Q[u]}" at r-(0,1.35*H);

    draw [font=\scriptsize\it,align=center]
      text "remote\\[-1pt]writing" at r+(1.5*W,0);

    var dx = 0.04, dy = 0.07;
    goto r+(0, 2.35*R) with [line width=1pt,color=white] {
      draw line (dx, dy) -- -(dx, dy);
      draw line (dx,-dy) -- -(dx,-dy);
    }
    goto r+(0, 2.35*R) with [line width=0.7pt,color=xred] {
      draw line (dx, dy) -- -(dx, dy);
      draw line (dx,-dy) -- -(dx,-dy);
    }
  }

  goto (2*X, 1.6*H) {
    draw circle r   radius R;
    draw circle v10 radius R;
    draw circle v01 radius R;

    var a = snd (r - v10);
    var b = fst (r - v10);
    var x = dis(r, v10) * sqrt(2);
    var cc = r + (a, -b);
    var deg = atan2(R, x);
    var d10 = atan2(snd(v10-cc), fst(v10-cc));
    var d01 = atan2(snd(v01-cc), fst(v01-cc));

    draw [->,densely dashed] arc cc radius x
      range (d10-deg) : (d01+deg);

    edge(v10, r);
    edge(r, v01);
    del(v10, r);

    draw text "$u$" at v10;
    //draw text "\texttt{P}" at v10+p;
    draw text "\texttt{P}" at r+p;
    draw text "\texttt{P}" at r+(-0.5*W,0.6*H);
    draw [font=\scriptsize] 
      text "\texttt{P[u] := P[P[u]]}" at r-(-0.65*W,0.7*H);

    draw [font=\scriptsize\it,align=center]
      text "pointer\\[-1pt]jumping" at r+(1.5*W,0);
  }
}