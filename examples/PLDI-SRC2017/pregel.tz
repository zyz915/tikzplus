tikz {
  \usepackage{mathpazo}

  def pregel() {
    var w = 0.1, t = -0.42;
    with [line width=1pt] {
      draw rect (0-w, t) to (0+w, -3.0);
      draw rect (1-w, t) to (1+w, -3.3);
      draw rect (2-w, t) to (2+w, -3.6);
      draw rect (3-w, t) to (3+w, -3.5);
      draw rect (4-w, t) to (4+w, -3.0);
      draw rect (5-w, t) to (5+w, -3.6);
      draw rect (6-w, t) to (6+w, -3.2);
    }
    draw [outer sep=0,inner sep=0,font=\footnotesize]
      text "processors" at (3, 0.37+t);

    goto (6, -2) {
      var v1 = (cos(-20), sin(-20));
      var v2 = (cos(-60), sin(-60));
      with [line width=0.8pt] {
        draw circle (0, 0) radius 0.2;
        with [densely dashed] {
          draw line v1*0.2 -- v1*2.5;
          draw line v2*0.2 -- v2*1.8;
        }
      }
    }

    goto (0, 0.2) {
      with [->,line width=0.7pt] {
        draw line (0, -4) -- (2, -6);
        draw line (1, -4) -- (0, -6);
        draw line (1, -4) -- (5, -6);
        draw line (2, -4) -- (5, -6);
        draw line (4, -4) -- (1, -6);
        draw line (4, -4) -- (3, -6);
        draw line (4, -4) -- (6, -6);
        draw line (6, -4) -- (4, -6);
      }

      draw [line width=1.6pt] line (-0.5, -6.3) -- (6.5, -6.3);

      var wd = -2.4;
      with [align=center,outer sep=0,inner sep=0,font=\scriptsize] {
        //draw text "local\\[-0.7ex]computation" at (wd, -1.7);
        //draw text "data\\[-0.7ex]communication" at (wd, -4.8);
        //draw text "barrier\\[-0.7ex]synchronization" at (wd, -6.4);
        draw [font=\small] text "BSP superstep" at (3, -6.9);
      }
    }

    goto (10, -4.2)
    {
      var m = 0.6;

      draw [line width=0.8pt] circle (0, 0) radius 0.55;
      draw text "$v$" at (0, 0);
      with [font=\scriptsize\strut] {
        draw node [left] at (-m, 0) text "messages";
        draw node [right] at ( m, 0) text "messages";
        draw text "\textit{compute()}" at (0, -1);
      }
      draw [font=\footnotesize,align=center]
        text "vertex-centric\\[-0.2em]computation" at (0, -2.1);

      with [->,line width=1.1pt] {
        draw line (-1.8, 0.65) -- (-0.6, 0.35);
        draw line (-1.8, -0.65) -- (-0.6, -0.35);
        draw line (0.65, 0.3) -- (1.8, 0.55);
        draw line (0.65, -0.3) -- (1.8, -0.55);
      }

      draw [font=\small,align=center]
        text "Bulk-Synchronous\\Parallel (BSP) model" at (0, 3.1);
    }

    draw [font=\small]
      text "Pregel = BSP + Vertex-Centric Approach" at (6.4, 0.9);
    draw [line width=0.4pt,dashed] line (0, 0.36) -- (13, 0.36)
  }

  scale 0.45 pregel()
}