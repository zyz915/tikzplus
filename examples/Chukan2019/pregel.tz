tikz
{
	\usepackage{mathpazo}

	var v1 = (0.2, 1), v2 = (0, 3), v3 = (1.5, 2.1), v4 = (2, 4);
	var v5 = (2.5, 0.2), v6 = (3.3, 1.7), v7 = (4.5, 3), v8 = (4.5, 1);
	var R = 0.27;

	def vertex(pair v) {
		draw [fill=white,line width=1.2pt] circle v radius R;
	}

	def edge(pair v1, pair v2) {
		var a = R / dis(v1, v2);
		var b = R / dis(v1, v2);
		var vx = v1 * a + v2 * (1 - a);
		var vy = v2 * b + v1 * (1 - b);
		draw [line width=1.2pt] line vx -- vy;
	}

	def edge_hide(pair v1, pair v2) {
		var a = R / dis(v1, v2);
		var vx = v1 * a + v2 * (1 - a);
		var vy = v2 * a + v1 * (1 - a);
		draw [densely dashed,line width=0.8pt] line vx -- vy;
	}

	fun unit(float deg): pair = (cos(deg), sin(deg))

	vertex(v1);
	vertex(v2);
	vertex(v3);
	vertex(v4);
	vertex(v5);
	vertex(v6);
	vertex(v7);
	vertex(v8);

	edge(v1, v2);
	edge(v1, v5);
	edge(v2, v4);
	edge(v3, v2);
	edge(v3, v4);
	edge(v3, v5);
	edge(v3, v6);
	edge(v3, v7);
	edge(v4, v7);
	edge(v5, v6);
	edge(v5, v8);
	edge(v6, v7);
	edge(v6, v8);
	edge(v7, v8);

	goto v7 + (3.1, 0.7)
	{
		draw [line width=1.2pt] circle (0, 0) radius 0.40;
		draw [font=\large] text "$v$" at (0, 0);

		with [font=\normalsize\strut] {
			var m = 0.6;
			draw node [left] at (-m, 0) text "messages";
			draw node [right] at ( m, 0) text "messages";
			draw text "\textit{compute()}" at (0, -0.8);
		}

		with [->,line width=1.3pt] {
			draw line (-1.5, 0.50) -- (-0.5, 0.25);
			draw line (-1.5, -0.50) -- (-0.5, -0.25);
			draw line (0.45, 0.20) -- (1.5, 0.45);
			draw line (0.45, -0.20) -- (1.5, -0.45);
		}
	}

	goto v7 with [densely dashed,line width=1pt] {
		var d1 = -5, d2 = 55
		draw line unit(d1) * R -- unit(d1) * R * 5.4;
		draw line unit(d2) * R -- unit(d2) * R * 4.6;
	}
}