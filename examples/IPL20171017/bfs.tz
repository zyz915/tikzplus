tikz [font=\footnotesize] {

    \usepackage{mathpazo}
    \usetikzlibrary{snakes}
    \usetikzlibrary{patterns}

    var V = 1.5;
    var H = -V;
    var u  = (1*V, 0);
    var v1 = (2*V, 0);
    var v2 = (3*V, 0);
    var R  = 0.22;

    var u_ex = u - (0, 0.4);

    def vertex_u() {
        draw [fill=white] circle  u radius R;
        draw [font=\scriptsize\strut] node at u text "$u$";
    };

    def vertex_v(pair v1, pair v2) {
        draw [fill=white] circle  v1 radius R;
        draw [font=\scriptsize\strut] node at v1 text "$v_1$";
        draw [fill=white] circle  v2 radius R;
        draw [font=\scriptsize\strut] node at v2 text "$v_n$";
        var mid = (v1 + v2)/2;
        draw node at mid text "$\cdots$";
        draw [decoration={brace,mirror,raise=0.3},decorate]
            line (v1+(-0.1,-0.3)) -- (v2+(0.1,-0.3));
        draw node [font=\tiny] at mid + (0, -0.5) text "$u$'s neighbours";
    };

    def vertex_v2() {
        draw circle  v2 radius R;
        draw node at v2 text "$v_n$";
    };

    def sep_line() {
        draw [dashed] line (-0.5, -V+0.4) -- (0.4+3*V, -V+0.4);
    };

    def cut(float r, pair v1, pair v2) {
        var v3 = v1 * (1.0 - r) + v2 * r;
        draw [->] line v1 -- v3;
    };

    var E0 = 1*V - R;
    var E1 = fst(v1-u) - R;
    var E2 = fst(v2-u) - R;
    var adj= (0.04, 0.04);
    var adj2= (0.04, -0.04);
    var hf = V/2;

    // borders
    draw [white] rect (-0.5, H+0.25) to (0.4+3*V,3*H);

    // state 1
    goto (0, H) {
        draw [below right,font=\tiny,align=center] text 
            multiline {
                "$R[u]$: reachable or not,",
                "initally set to false"
            } at v1+(-0.3,0.15);
        draw node at (0, 0) text "state 1";
        draw node [font=\tiny] at (0, -0.24) text "initialization";
        draw [->] line (0, -0.24-R) -- (0, R-V);
        vertex_u();
        goto (-0.75, -0.44) with [font=\tiny\strut] {
            draw node [right] at u+(-0.2,0)
                text "if ($getId()=S$)";
            draw node [right] at u+(0,-0.24)
                text "$R[u] \leftarrow true$";
            draw node [right] at u+(0,-0.48)
                text "$sendMessages()$";
        };
        draw [->] line (u+(R,-R)+adj*1) -- (u+(E1,-E0));
        draw [->] line (u+(R,-R)+adj*2) -- (u+(E2,-E0));
        sep_line();
    };

    // state 2
    goto (0, 2*H) {
        draw node at (0, 0) text "state 2";
        draw node [font=\tiny] at (0, -0.24) text "propagation";
        draw [->] line (0, -0.24-R) -- (0, R-V);
        //draw [->] line (-0.4, 0.035) -- (-0.9, 0.035) -- (-0.9, 3*H)-- (-0.4, 3*H);
        vertex_u();
        goto (-0.75, -0.44) with [font=\tiny\strut] {
            draw node [right] at u+(-0.2,0)
                text "if ($hasMsg()\wedge\\!R[u]$)";
            draw node [right] at u+(0,-0.24)
                text "$R[u] \leftarrow true$";
            draw node [right] at u+(0,-0.48)
                text "$sendMessages()$";
        };
        draw [->] line (u+(R,-R)+adj*1) -- (u+(E1,-E0));
        draw [->] line (u+(R,-R)+adj*2) -- (u+(E2,-E0));
        vertex_v(v1, v2);
        sep_line();
    };

    // state 3
    goto (0, 3*H) {
        draw node at (0, 0) text "state 2";
        draw node [font=\tiny] at (0, -0.24) text "propagation";
        draw [->,densely dashed] line (0, -0.24-R) -- (0, R-0.7*V);
        draw [font=\tiny] text "(repeat)" at (0, R-0.8*V);
        vertex_u();
        vertex_v(v1, v2);
        goto (-0.75, -0.44) with [font=\tiny\strut] {
            draw node [right] at u+(-0.2,0)
                text "if ($hasMsg()\wedge\\!R[u]$)";
            draw node [right] at u+(0,-0.24)
                text "$R[u] \leftarrow true$";
            draw node [right] at u+(0,-0.48)
                text "$sendMessages()$";
        };
        cut(0.7, (u+(R,-R)+adj*1), (u+(E1,-E0)));
        cut(0.7, (u+(R,-R)+adj*2), (u+(E2,-E0)));
    };

}
