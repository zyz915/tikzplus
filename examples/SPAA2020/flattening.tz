tikz [line width=0.7pt]
{
	\usepackage{mathpazo}

	\definecolor{lblue}{RGB}{174,199,232}
	\definecolor{lorange}{RGB}{255,158,74}
	\definecolor{lgreen}{RGB}{103,191,92}
	\definecolor{lred}{RGB}{237,102,93}
	\definecolor{lgray}{RGB}{220,220,220}

	var k = 1.7, r = 0.18;
	var v0 = (0, -k);
	var v1 = (0, 0), v2 = (1, 0), v5 = (1+k, 0);
	var v4 = (0, -k*0.5), v8 = (1, -k*0.5), v3 = (1+k*0.5, -k*0.5);
	var v6 = (1, -k), v7 = (1+k, -k);

	draw [white] rect (-0.3, 0.5) to (3, -2)

	def edge(pair v1, pair v2, int w) {
		var t = r / dis(v1, v2)
		var p = v1 * (1 - t) + v2 * t;
		var q = v1 * t + v2 * (1 - t);
		draw [line width=0.2pt,color=gray] line p -- q;
		var c = (v1 + v2) * 0.5;
		var d = 0.5 / dis(v1, v2);
		var r = v2 - v1
		var x = (-snd(r), fst(r)) * d
		// draw [font=\tiny] text str(w) at c + x * 0.2
	}

	fun atan2x(pair v): float = atan2(snd(v), fst(v));

	def arrow(pair v1, pair v2, int w, string s = "->") {
		var t = r / dis(v1, v2)
		var p = v1 * (1 - t) + v2 * t;
		var q = v1 * t + v2 * (1 - t);
		draw [@s] line p -- q;
		var c = (v1 + v2) * 0.5;
		var d = 0.5 / dis(v1, v2);
		var r = v2 - v1
		var x = (-snd(r), fst(r)) * d
		// draw [font=\tiny] text str(w) at c + x * 0.2
	}

	def arrow2(pair v1, pair v2, int w) {
		var m = (v1 + v2) * 0.5;
		var d = dis(v1, v2) * 0.8;
		var t = v2 - v1
		var x = (snd(t), -fst(t)) * d;
		var c1 = m + x;
		var R = dis(v1, c1) + r * 0.05
		var deg = 180.0 * r / (R * 3.14159265358979323846);
		var d1 = atan2x(v1 - c1) - deg;
		var d2 = atan2x(v2 - c1) + deg;
		draw [<-] arc c1 radius R range d2:d1;
		var c2 = m - x;
		var d3 = atan2x(v1 - c2) + deg;
		var d4 = atan2x(v2 - c2) - deg;
		draw [<-] arc c2 radius R range d3:d4;
		draw [font=\tiny] text str(w) at m
	}

	def root(pair v) {
		draw [<-] arc v+(0,r*1.6) radius r*0.8 range -40:220
	}

	arrow(v0, v4, 4)
	arrow(v1, v2, 2)
	arrow(v2, v5, 1, "<-")
	arrow(v7, v3, 2)
	arrow(v4, v8, 1, "<-")
	arrow(v4, v6, 2, "<-")

	edge(v1, v4, 4)
	edge(v2, v3, 5)
	edge(v2, v8, 3)
	edge(v3, v5, 4)
	edge(v3, v6, 3)
	edge(v5, v7, 2)
	edge(v8, v6, 6)
	edge(v0, v6, 5)

	comment {
		for v in (v1, v2, v5) do
			draw [fill=lblue] circle v radius r;

		for v in (v3, v7) do
			draw [fill=lorange] circle v radius r;

		for v in (v0, v4, v6, v8) do
			draw [fill=lgreen] circle v radius r;
	}

	for v in (v0, v1, v5, v6, v7, v8) do
		draw circle v radius r;

	for v in (v2, v3, v4) do
		draw [fill=lgray] circle v radius r;

	for v in (v2, v3, v4) do
		root(v);

	with [font=\scriptsize] {
		draw text "0" at v0
		draw text "1" at v1
		draw text "2" at v2
		draw text "3" at v3
		draw text "4" at v4
		draw text "5" at v5
		draw text "6" at v6
		draw text "7" at v7
		draw text "8" at v8
	}
}