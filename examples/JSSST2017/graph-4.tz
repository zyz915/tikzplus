tikz [line width=1pt,font=\small]
{
	\usepackage{mathpazo}

	var v1 = (-0.6, 1.6);
	var v2 = (-1.4, 0);
	var v3 = (0, 0);
	var v4 = (1.2, 0.4);
	var v5 = (-0.2, -1.6);
	var R  = 0.12, mv = 0.32;

	def edge(pair v1, pair v2) {
		var l = dis(v2, v1), r = R * 1.1;
		var p1 = v2 * r / l + v1 * (l - r) / l;
		var p2 = v1 * r / l + v2 * (l - r) / l;
		draw [->] line p1 -- p2;
	}

	def conn(pair v1, pair v2) {
		var l = dis(v2, v1), r = R * 1.1;
		var p1 = v2 * r / l + v1 * (l - r) / l;
		var p2 = v1 * r / l + v2 * (l - r) / l;
		draw [dashed,->] line p1 -- p2;
	}

	fun move(float l, float deg): pair =
		(l*cos(deg), l*sin(deg));

	draw [fill=black] circle v1 radius R;
	draw text "$v_1$" at v1+move(mv, 160);
	draw [fill=black] circle v2 radius R;
	draw text "$v_2$" at v2+move(mv, 200);
	draw [fill=black] circle v3 radius R;
	draw text "$v_3$" at v3+move(mv, 70);
	draw circle v4 radius R;
	draw text "$v_4$" at v4+move(mv*0.9, 90);
	draw [fill=black] circle v5 radius R;
	draw text "$v_5$" at v5+move(mv, 180);

	edge(v1, v3);
	edge(v2, v1);
	edge(v2, v3);
	edge(v3, v5);
	conn(v4, v3);
	conn(v4, v5);
	edge(v5, v2);
}
