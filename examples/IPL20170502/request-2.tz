tikz [line width=1pt]
{
	\usepackage{mathpazo}

	fun getv(float R, int i): pair =
		let ang = 360.0 / 8 * (i + 0.5) in
		(R * cos(ang), R * sin(ang));

	def person(pair u) {
		var r = 0.2, v = u + (0, 0.75*r);
		var mx = 1.35 * r, my = 2.5 * r;
		draw [rounded corners] line v -- v + (-mx, -my) -- v + (mx, -my) -- v;
		draw [fill=white] circle v radius r;
	}

	var R = 2.0;
	def arrow(pair vi, pair vj, int response) {
		var p = vj - vi, d = dis(vi, vj), u = p / d;
		var mv = (-snd(u), fst(u)) * 0.06;
		var ho = 2-fabs(fst(u));
		var x = ho * 0.2, y = 0.02;
		draw [line width=0.9pt,->] line vi + x*u+p*y -- vi + (d-x)*u-p*y;
		if (response)
			draw [line width=0.6pt,->] line vj+mv-(x+0.15)*u-p*y -- vj+mv-(x+0.5)*u-p*0.1;
	}

	var p1 = (-0.5,  0),   p2 = ( 0.1, 0.5);
	var p3 = ( 0.2, -0.5), p4 = ( 0.1, 0);
	var e = 1.0, blk = (e*1.1, 0);

	def draw_blk() {
		var w = e * 0.18, h = e * 0.16;
		draw [fill=white] rect blk-(w, h) to blk+(w, h);
	}

	def worker_left() {
		draw text "Worker" at (0, e*1.4);
		draw rect (-e, -e) to (e, e);
		draw rect (-e*1.2, -e*1.2) to (e*1.2, e*1.8);
		person(p1);
		person(p2);
		person(p3);
	}

	def worker_right() {
		var e = 1.0;
		draw text "Worker" at (0, e*1.4);
		draw rect (-e, -e) to (e, e);
		draw rect (-e*1.2, -e*1.2) to (e*1.2, e*1.8);
		person(p4);
		draw [font=\scriptsize] text "1 request" at p4+(0,0.6);
	}

	var w = 1.5;
	goto (-w, 0) worker_left();
	goto ( w, 0) worker_right();
	goto (-w, 0) draw_blk();

	arrow(p1-(w,0), blk-(w,0), 0);
	arrow(p2-(w,0), blk-(w,0), 0);
	arrow(p3-(w,0), blk-(w,0), 0);

	arrow(blk-(w,0), p4+(w,0), 1);
}
