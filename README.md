## Introduction ##

This repository is to create a Domain-Specific Language called **tikz+** for drawing high quality figures (vector graphics).
It compiles to latex source code with the tikz package.
The main features include:

1. Variables, type system and arithmetic expressions
2. Basic shapes (line, circle, cycle, rectangle, arc, text, etc.)
3. Control flows (branch, loop, function)
4. Coordinate transformation primitives
5. Build-in functions

## Installation ##

### The Compiler ###

The compiler requires the following environments:

1. [cmake](https://cmake.org/)
2. a c++ compiler (C++11 compatible)

The following steps build the compiler (for Mac OS X and Ubuntu):

```
$ cd $(top)
$ cmake .
$ make
```

The compiler is a single executable `./tikz`

### Syntax Highlighting for vim ###

Vim users can enable syntax highlighting of Tikz+ programs (.tz files) through the following steps:

```
cp $(top)/vim/syntax/tikzplus.vim ~/.vim/syntax/
cp $(top)/vim/ftdetect/tikzplus.vim ~/.vim/ftdetect/
```

## Tutorial ##

### Hello World ###

First, let's create a file `hello.tz` in the top-level folder and write the following program:

```
tikz {
  // print "Hello World"
  draw text "Hello World" at (0, 0);
}
```

A tikz+ program has the form of `tikz { ... }`, inside which we can use the "draw" command to produce some content in the figure.
The basic command is creating some text at a certain coordinate.
Tikz+ supports single-line comments, which starts with `//` and continue until the end of line.

The compilation is done by the following command

```
./tikz hello.tz
```

Then, an output file `hello.tex` is generated in the same folder, which looks like this:

```
\documentclass[border=1pt]{standalone}
\usepackage[dvipsnames]{xcolor}
\usepackage{tikz}
\begin{document}
\begin{tikzpicture}
\draw (0, 0) node {Hello World};
\end{tikzpicture}
\end{document}
```

This is a latex standalone document with two packages `xoclor` and `tikz` loaded by default.
In this file, we create a single node at the origin (which is by default invisible), and print the text `"Hello World"` there.
You can further generate the figure by the following command:

```
pdflatex hello.tex
```

which generates `hello.pdf` in the current folder. Alternatively, you can also use `latex` to generate dvi file.

### Data types ###

Tikz+ has four data types: **int**, **float**, **pair** and **string**.
The **pair** represents a coordinate in the 2-dimensional space, which consists of a pair of float numbers.
Here, an implicit type conversion will be automatically performed when integer values are provided.

Please note that the boolean values are represented by integer numbers where a non-zero number is regarded as **true**.
We also provide two build-in constants **true** and **false**, which are equivalent to 1 and 0 respectively.

In a tikz+ file, you can define and use the variables like this:

```
tikz {
  // variable definitions
  var x1 = 0,   y1 = 1,    v1 = (x1, y1);
  var x2 = 0.5, y2 = -0.5, v2 = (x2, y2);
  var s = "Hello World";

  draw text s at v1;
  draw text s at v2;
}
```

where a variable definition is a statement starting with keyword **var** consists of several bindings.
To prevent potential mistakes, a restriction is that you cannot define the same variable in the same scope twice in the program.

### Shapes ###

Tikz+ supports the following shapes: **line**, **cycle**, **rect**, **circle**, **arc** and **text**.
Here we present how to draw these shapes with proper parameters.

#### 1. Line and Cycle ####

The syntax for line and cycle are as below:

```
tikz {
  draw line (-1, 0) -- (1, 0);
  draw [->] line (-1, 0.5) -- (1, 0.5);
  draw [double] line (-1, 1) -- (1, 1);
  draw [dotted,thick] line (-1, 1.5) -- (1, 1.5);
  draw [dashed,blue] line (-1, 2) -- (1, 2);
  draw [rounded corners] cycle (2, 0) -- (2, 1) -- (3, 1) -- (3, 0);
}
```

The **line** command specifies a sequence of coordinates, and then line segments are created to connect the neighboring nodes.
The **cycle** is basically the same, except that an extra line segment is created to connect the first and last coordinates in the sequence.

There are a bunch of options to customize the line segments, as shown in this example, like arrow (->), color, thickness or others.
These options are enclosed by the brackets and attached after the keyword **draw**.
The compilation result is exactly the `\draw` command with those parameters attached.
For more information about customizing the lines, please refer to tikz package's manual.

#### 2. Rectangle ####

We can draw a rectangle by specifying two diagonal coordinates using the following syntax:

```
tikz {
  draw [rounded corners,dashed,thick] rect (0, 0) to (1, 1);
  draw [thick,pattern=north west lines] rect (1, 1) to (2, 2);
}
```

Like we did before, we can also add some options to customize the rectangle.
They will be directly attached to the corresponding `\draw` command as well.

#### 3. Circle and Arc ####

A circle is determined by the center point and the radius, while an arc has additional parameters specifying the range (in degree).
The syntax is shown below:

```
tikz {
  draw [fill=black!20] circle (0, 0) radius 1;
  draw [red] arc (0, 0) radius 0.95 range 90:180;
}
```

#### 4. Text ####

We have already seen the usage of text in the hello world example, and it compiles to tikz's drawing node command.
Similar to other shapes, we can parameterize a text node by attaching the configuration after the `draw` keyword.
Some useful parameters are shown below:

```
tikz {
  var s1 = "zero", s2 = "one\\[-0.3ex]two";
  // show the border line
  draw [draw,thick] text s1 at (0, 0);
  // change the font
  draw [font=\scriptsize\tt] text s1 at (0, 1);
  // print multiple lines
  draw [align=left] text s2 at (0, 2);
  // adjust the location
  draw [above left] text "three" at (0, 3);
  draw [below right] text "four" at (0, 3);
  // math environment
  draw text "$\sum_{i=0}^n$" at (0, 4);
}
```

### Control Flows ###

Tikz+ currently supports four control flows: if, for, procedure call and function.
Let's see the following program:

```
tikz {
  fun get(pair v, float l, float deg): pair =
    let dx = l * cos(deg)
        dy = l * sin(deg)
    in v + (dx, dy);

  def tri(int lvl, float l, pair v) {
    if (lvl == 0) {
    	draw cycle get(v, 2*l, 90) --
            get(v, 2*l, 210) -- get(v, 2*l, 330);
    } else {
      for i = 0 to 2 do {
        var deg = 90 + i * 120;
        var nv = get(v, l, deg);
        tri(lvl-1, l/2, nv);
      }
    }
  }

  tri(3, 3.0, (0, 0));
}
```

This program produce different sizes of triangles and arrange them in a specific pattern.
A procedure call always starts with the keyword **def**, and the definition of parameters and the body is in C style.
Then, a function starts with the keyword **fun**, followed by a list of parameters and a return type, and be careful that the function body is just an expression.
Tikz+'s **if** statement is exactly the same as C's if statement, however the **for** statement is to iterate between two integer values.
The build-in functions will be introduced later.

### Coordinate Transformation ###

An interesting feature of tikz+ is the ability to directly perform coordinate transformation on an entire figure described by a piece of code.
For example, we can easily move a figure to another place by using the **anchor** keyword;
we can rotate a figure by using the **rotate** keyword followed by a float degree; or we can adjust the size of a figure by using the keyword **scale**.
All of them are just achieved by a simple statement.
Here is an example:

```
tikz {
  def try(int k, float d) {
    draw cycle (1, 1) -- (-1, 1) -- (-1, -1) -- (1, -1);
    if (k != 0) {
      var s = fabs(cos(d)) + fabs(sin(d));
      scale 1/s rotate d try(k-1, d);
    }
  }
  goto ( 1.2, 0) try(12, -15);
  goto (-1.2, 0) try(12,  15);
}
```

### Build-in Functions ###

Tikz+ currently supports the following build-in functions:

1. trigonometric functions: **cos**(x), **sin**(x), **tan**(x), **atan2**(y, x)
2. math functions: **abs**(x), **fabs**(x), **sqrt**(x)
3. comparison: **max**(x, y), **min**(x, y)
4. type casting: **int**(x), **float**(x)
5. component of pair type: **fst**(x), **snd**(x)
6. distance of two coordinates: **dis**(x, y)
7. string related functions: **str**(x), **concat**(s1, s2)

## Contact ##

Yongzhe Zhang (Ph.D. student)  
The Graduate University of Advanced Studies (SOKENDAI), Japan  
National Institute of Informatics  
Email: zyz915@gmail.com
