if exists("b:current_syntax")
    finish
endif

let b:current_syntax = "tikzplus"

syntax keyword keywords  tikz def fun var goto rotate scale if else for to downto do let in with as comment multiline
syntax keyword keywords  draw line cycle rect circle radius arc range node at text
syntax keyword types     int float pair string option
syntax keyword values    true false
syntax keyword functions abs fabs sin cos tan atan2 sqrt dis max min fst snd str concat

syn match comments "//.*$"
syn match intstr   '\d\+'
syn match intstr   '[-+]\d\+'
syn match floatstr '\d\+\.\d*'
syn match floatstr '[-+]\d\+\.\d*'
syn match varstr   '\a\w*'
syn match option   '\[.*\]'
syn match sstring  '\".*\"'
syn match header   "\\\a*"

highlight link keywords  Keyword
highlight link types     Type
highlight link values    Constant
highlight link intstr    Number
highlight link floatstr  Float
highlight link functions Keyword
highlight link comments  Comment
highlight link header    Include
