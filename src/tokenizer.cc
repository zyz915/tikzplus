#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <vector>

#include "errorinfo.h"
#include "tokenizer.h"

using namespace std;

static int line = 0;

static char buff[256], br_stack[256];
static int bptr, br_top = 1;
static int no_tok;

static char buf_[512];

static FILE *fin = stdin;
void setInputStream(FILE *f) {
	if (f == NULL)
		prtError("input file does not exist");
	fin = f;
}

static void getLine() {
	line += 1;
	bptr = 0;
	no_tok = (fgets(buff, 512, fin) == NULL);
	setline(line, buff);
	if (!no_tok)
		while (buff[bptr] && isspace(buff[bptr]))
			bptr++;
}

static Token createToken(int row, int col, int type, const char *val = "") {
	return Token(row, col + 1, type, val);
}

static void copyString(char *dst, const char *src, int len) {
	memcpy(dst, src, len);
	dst[len] = 0;
}

typedef pair<const char *, int> TokRec;
static vector<TokRec> names;

static bool cmp(const TokRec &a, const TokRec &b) {
	return strcmp(a.first, b.first) < 0;
}

void initTokenizer() {
	names.clear();
	names.push_back(make_pair("tikz", tok_tikz));
	names.push_back(make_pair("def", tok_def));
	names.push_back(make_pair("fun", tok_fn));
	names.push_back(make_pair("var", tok_var));
	names.push_back(make_pair("int", tok_int));
	names.push_back(make_pair("float", tok_float));
	names.push_back(make_pair("pair", tok_pair));
	names.push_back(make_pair("string", tok_string));
	names.push_back(make_pair("option", tok_option));
	names.push_back(make_pair("anchor", tok_anchor));
	names.push_back(make_pair("goto", tok_anchor));
	names.push_back(make_pair("rotate", tok_rotate));
	names.push_back(make_pair("scale", tok_scale));
	names.push_back(make_pair("with", tok_with));
	names.push_back(make_pair("comment", tok_comment));
	names.push_back(make_pair("if", tok_if));
	names.push_back(make_pair("else", tok_else));
	names.push_back(make_pair("for", tok_for));
	names.push_back(make_pair("to", tok_to));
	names.push_back(make_pair("downto", tok_downto));
	names.push_back(make_pair("do", tok_do));
	names.push_back(make_pair("let", tok_let));
	names.push_back(make_pair("in", tok_in));
	names.push_back(make_pair("draw", tok_draw));
	names.push_back(make_pair("line", tok_line));
	names.push_back(make_pair("cycle", tok_cycle));
	names.push_back(make_pair("rect", tok_rect));
	names.push_back(make_pair("circle", tok_circle));
	names.push_back(make_pair("radius", tok_radius));
	names.push_back(make_pair("arc", tok_arc));
	names.push_back(make_pair("range", tok_range));
	names.push_back(make_pair("node", tok_node));
	names.push_back(make_pair("at", tok_at));
	names.push_back(make_pair("text", tok_text));
	names.push_back(make_pair("as", tok_as));
	names.push_back(make_pair("multiline", tok_multi));
	names.push_back(make_pair("true", tok_true));
	names.push_back(make_pair("false", tok_false));
	names.push_back(make_pair("abs", tok_abs));
	names.push_back(make_pair("fabs", tok_fabs));
	names.push_back(make_pair("sin", tok_sin));
	names.push_back(make_pair("cos", tok_cos));
	names.push_back(make_pair("tan", tok_tan));
	names.push_back(make_pair("atan2", tok_atan2));
	names.push_back(make_pair("sqrt", tok_sqrt));
	names.push_back(make_pair("dis", tok_dis));
	names.push_back(make_pair("fst", tok_fst));
	names.push_back(make_pair("snd", tok_snd));
	names.push_back(make_pair("max", tok_max));
	names.push_back(make_pair("min", tok_min));
	names.push_back(make_pair("str", tok_str));
	names.push_back(make_pair("concat", tok_concat));
	sort(names.begin(), names.end(), cmp);
}

static Token lookupName(char *buf, int col) {
	int lo = -1, hi = names.size(), mid;
	while (lo + 1 < hi) {
		mid = (lo + hi) >> 1;
		int c = strcmp(buf, names[mid].first);
		if (c == 0)
			return createToken(line, col,
				names[mid].second, strdup(buf));
		if (c < 0)
			hi = mid;
		else
			lo = mid;
	}
	return createToken(line, col, tok_name, strdup(buf));
}

enum modeID { m_normal, m_option, m_string };
static modeID mode = m_normal;
static Token getnext();

static Token getopt() {
	while (buff[bptr] == ' ' || buff[bptr] == '\t')
		bptr++;
	int ptr = 0, col = bptr, lvl = 0;
	if (buff[bptr] == ',') {
		bptr++;
		return createToken(line, col, tok_comma, NULL);
	}
	if (buff[bptr] == ']') {
		bptr++;
		mode = m_normal;
		return createToken(line, col, tok_rbracket, NULL);
	}
	if (buff[bptr] == '@') {
		bptr++;
		while (isalnum(buff[bptr]) || buff[bptr] == '_')
			bptr++;
		if (col + 1 == bptr)
			prtError(Pos(line, col), "no variable is found");
		copyString(buf_, buff + col, bptr - col);
		return createToken(line, col, tok_link, strdup(buf_));
	}
	while (true) {
		char ch = buff[bptr];
		if (ch == '{') lvl++;
		if (ch == '}') lvl++;
		if (ch == ']' || (!lvl && ch == ',')) {
			buf_[ptr] = 0;
			return createToken(line, col, tok_spec, strdup(buf_));
		}
		if (ch == '\n')
			prtError(Pos(line, col), "unexpected newline");
		buf_[ptr++] = ch;
		bptr++;
	}
}

static Token getstr(char end) {
	int ptr = 0, col = bptr-1;
	buf_[ptr++] = end;
	while (true) {
		char ch = buff[bptr];
		if (ch == end) {
			buf_[ptr++] = ch;
			buf_[ptr] = 0;
			mode = m_normal;
			bptr++; // ignore the '"'
			return createToken(line, col, tok_sval, strdup(buf_));
		}
		if (ch == '\n') prtError(Pos(line, col), "unexpected newline");
		buf_[ptr++] = ch;
		bptr++;
	}
}

static Token gettok() {
	static char buf_[32];
	Token ret;
	while (!no_tok && !buff[bptr])
		getLine();
	if (no_tok)
		return createToken(line, 0, tok_eof);

	while (true) {
		char ch = buff[bptr];
		int col = bptr++;
		if (ch == 0) {
			getLine();
			return getnext(); // reach EOL
		}
		if (isspace(ch))
			continue;
		if (isalpha(ch)) {
			while (isalnum(buff[bptr]) || buff[bptr] == '_') bptr++;
			copyString(buf_, buff + col, bptr - col);
			return lookupName(buf_, col);
		}
		if (isdigit(ch)) {
			while (isdigit(buff[bptr]))
				bptr++;
			if (buff[bptr] == '.') {
				bptr++;
				while (isdigit(buff[bptr])) bptr++;
				copyString(buf_, buff + col, bptr - col);
				return createToken(line, col, tok_fval, strdup(buf_));
			} else {
				copyString(buf_, buff + col, bptr - col);
				return createToken(line, col, tok_ival, strdup(buf_));
			}
		}
		switch (ch) {
		case '\\':
			while (buff[bptr] != '\n') bptr++;
			copyString(buf_, buff + col, bptr - col);
			ret = createToken(line-1, col, tok_head, strdup(buf_));
			getLine();
			return ret;
		case '"':
			mode = m_string;
			return getstr('"');
		case '\'':
			mode = m_string;
			return getstr('\'');
		case '[':
			mode = m_option;
			return createToken(line, col, tok_lbracket);
		case ']':
			if (br_stack[br_top] != '[')
				prtError(Pos(line, col), "mismatched brackets");
			br_top -= 1;
			return createToken(line, col, tok_rbracket);
		case '(':
			br_stack[++br_top] = '(';
			return createToken(line, col, tok_lparen);
		case ')':
			if (br_stack[br_top] != '(')
				prtError(Pos(line, col), "mismatched parenthesis");
			br_top -= 1;
			return createToken(line, col, tok_rparen);
		case '{':
			br_stack[++br_top] = '{';
			return createToken(line, col, tok_lbrace);
		case '}':
			if (br_stack[br_top] != '{')
				prtError(Pos(line, col), "mismatched braces");
			br_top -= 1;
			return createToken(line, col, tok_rbrace);
		case '<':
			if (!strncmp(buff + col, "<=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_le);
			} else
			if (!strncmp(buff + col, "<<", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_shl);
			} else
				return createToken(line, col, tok_lt);
		case '>':
			if (!strncmp(buff + col, ">=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_ge);
			} else
			if (!strncmp(buff + col, ">>", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_shr);
			} else
				return createToken(line, col, tok_gt);
		case '+':
			return createToken(line, col, tok_add);
		case '-':
			if (!strncmp(buff + col, "--", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_conn);
			} else
				return createToken(line, col, tok_sub);
		case '*':
			return createToken(line, col, tok_mul);
		case '/':
			if (!strncmp(buff + col, "//", 2)) {
				while (buff[bptr] != '\n') bptr++;
				copyString(buf_, buff + col, bptr - col);
				ret = createToken(line-1, col, tok_comm, strdup(buf_));
				getLine();
				return ret;
			}
			return createToken(line, col, tok_div);
		case '%':
			return createToken(line, col, tok_mod);
		case '=':
			if (!strncmp(buff + col, "==", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_eq);
			} else
				return createToken(line, col, tok_bind);
		case '&':
			if (!strncmp(buff + col, "&&", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_and);
			} else
				prtError(Pos(line, col), "unknown operator '&'; do you mean '&&'?");
		case '|':
			if (!strncmp(buff + col, "||", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_or);
			} else
				prtError(Pos(line, col), "unknown operator '|'; do you mean '||'?");
		case '!':
			if (!strncmp(buff + col, "!=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_ne);
			} else
				return createToken(line, col, tok_not);
		case '?':
			return createToken(line, col, tok_ques);
		case ':':
			return createToken(line, col, tok_colon);
		case ';':
			return createToken(line, col, tok_semicol);
		case ',':
			return createToken(line, col, tok_comma);
		}
	}
	return createToken(line, 0, tok_eof);
}

static Token getnext() {
	switch (mode) {
	case m_normal:
		return gettok();
	case m_option:
		return getopt();
	case m_string:
		prtError("unexpected state");
		return getstr('"');
	}
}

Token CurTok;
Token getNextToken(bool comm) {
	while (true) {
		CurTok = getnext();
		if (comm || CurTok.type != tok_comm)
			return CurTok;
	}
}

static const char *token_strs[] = {
	"", "EOF", "comment", "integer", "float number", "string",
	"parameter", "header", "identifier", "link",
	"[", "]", "(", ")", "{", "}", ",", "--", ":", ";", "?", "=",
	"+", "-", "*", "/", "%", "<", "<=", ">", ">=", "==", "!=",
	"&&", "||", "<<", ">>", "!",
	"tikz", "def", "fn", "var", "int", "float", "pair", "string", "option",
	"anchor", "rotate", "scale", "with", "comment",
	"if", "else", "for", "to", "downto", "do", "let", "in",
	"draw", "line", "cycle", "rect", "cricle", "radius",
	"arc", "range", "node", "at", "text", "as", "multiline", "true", "false",
	"abs", "fabs", "sin", "cos", "tan", "atan2", "sqrt", "dis",
	"fst", "snd", "max", "min", "str", "concat", ""
};

const char *getTokenStr(Token tok) {
	if ((tok.type >= tok_comm && tok.type <= tok_link))
		return tok.val;
	if ((tok.type >= tok_tikz && tok.val != NULL))
		return tok.val;
	return token_strs[tok.type];
}

const char *getTokenStr(int type) {
	return token_strs[type];
}
