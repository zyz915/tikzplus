#include <algorithm>

#include "ast.h"
#include "tokenizer.h"
#include "types.h"

using namespace std;

TypeEnv::TypeEnv() : size_(0), mx_(0) {
	push();
}

void TypeEnv::push() {
	st_.push_back(map<string, Type>());
}

void TypeEnv::pop() {
	size_ -= st_.back().size();
	st_.pop_back();
}

void TypeEnv::setType(const Identifier *name, Type t) {
	st_.back().insert(make_pair(name->getName(), t));
	mp_[name->getName()] = size_++;
	mx_ = max(mx_, size_);
}

Type TypeEnv::getType(const Identifier *name) const {
	for (auto mp : st_) {
		auto t = mp.find(name->getName());
		if (t != mp.end())
			return t->second;
	}
	return t_unit;
}

int TypeEnv::getIndex(const Identifier *name) const {
	auto t = mp_.find(name->getName());
	return (t == mp_.end() ? -1 : t->second);
}

int TypeEnv::size() {
	return mx_;
}

const char *getTypeStr(Type t) {
	switch (t) {
		case t_int: return "Int";
		case t_float: return "Float";
		case t_pair: return "Pair";
		case t_string: return "String";
		case t_unit: return "Unit";
	}
}

Type unifyType(Type t1, Type t2) {
	if (t1 == t2) return t1;
	Type t = max(t1, t2);
	if (t != t_float && t != t_int)
		return t_unit;
	return t;
}

