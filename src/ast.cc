#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <set>

#include "ast.h"
#include "errorinfo.h"
#include "eval.h"
#include "types.h"

using namespace std;

static TransSystem *trans = new TransSystem();
static char err[64];

static ExprAST* require(Type t, ExprAST *expr) {
	if (expr->type() != t) {
		sprintf(err, "require %s but %s is detected",
				getTypeStr(t), getTypeStr(expr->type()));
		prtTypeError(*expr, err);
	}
	return expr;
}

static ExprAST* cast(Type t, ExprAST *expr) {
	if (t == expr->type())
		return expr;
	if (t == t_float) {
		if (expr->type() != t_int) {
			sprintf(err, "no valid conversion from %s to Float",
					getTypeStr(expr->type()));
			prtError(*expr, err);
		}
		if (expr->id() == i_int)
			return new FloatExpr(*expr, ((IntExpr*) expr)->getval());
		return new UnaryExpr(new Operator(tok_float), expr, t_float);
	}
	sprintf(err, "no valid conversion from %s to %s",
			getTypeStr(expr->type()), getTypeStr(t));
	prtError(*expr, err);
	return NULL;
}

static void prtBinOpError(Operator *op, const char *msg) {
	sprintf(err, "'%s' %s", getTokenStr(op->type), msg);
	prtTypeError(*op, err);
}

OptionRef::OptionRef(Pos pos, Store *ref) : Option(pos) {
	ref_ = (Store*) require(t_string, ref);
}

MultiLine::MultiLine(Pos pos, std::string str, std::vector<ExprAST*> list) :
		ExprAST(pos, t_string, i_multi), opt_(str), list_(list) {
	for (int i = 0; i < list.size(); i++)
		list[i] = require(t_string, list[i]);
}

PairExpr::PairExpr(Pos pos, ExprAST *fst, ExprAST *snd) :
		ExprAST(pos, t_pair, i_pair) {
	fst_ = cast(t_float, fst);
	snd_ = cast(t_float, snd);
}

IfExpr::IfExpr(Pos pos, ExprAST *cond, ExprAST *e1, ExprAST *e2) :
		ExprAST(pos, t_unit, i_if) {
	cond_ = cast(t_int, cond);
	type_ = unifyType(e1->type(), e2->type());
	if (type_ == t_unit)
		prtError(pos, "type mismatch");
	e1_ = cast(type_, e1);
	e2_ = cast(type_, e2);
}

GotoStmt::GotoStmt(Pos pos, ExprAST *loc, StmtAST *stmt) :
		StmtAST(pos), stmt_(stmt) {
	loc_ = cast(t_pair, loc);
}

ScaleStmt::ScaleStmt(Pos pos, ExprAST *scale, StmtAST *stmt) :
		StmtAST(pos), stmt_(stmt) {
	scale_ = cast(t_float, scale);
}

RotateStmt::RotateStmt(Pos pos, ExprAST *deg, StmtAST *stmt) :
		StmtAST(pos), deg_(deg), stmt_(stmt) {
	deg_ = cast(t_float, deg);
}

IfStmt::IfStmt(Pos pos, ExprAST *cond, StmtAST *s1, StmtAST *s2) :
		StmtAST(pos), s1_(s1), s2_(s2) {
	cond_ = require(t_int, cond);
}

ForStmt::ForStmt(Pos pos, Store *lval, ExprAST *st,
		ExprAST *ed, int inc, StmtAST *body):
		StmtAST(pos), lval_(lval), inc_(inc), body_(body) {
	st_ = require(t_int, st);
	ed_ = require(t_int, ed);
}

IterStmt::IterStmt(Pos pos, Store *lval, std::vector<ExprAST*> list, StmtAST *body):
		StmtAST(pos), lval_(lval), list_(list), body_(body) {
	Type ty = list[0]->type();
	for (ExprAST *it : list_)
		require(ty, it);
}

Line::Line(Pos pos, std::vector<ExprAST*> list) :
		Object(pos), list_(list) {
	for (ExprAST* it : list_)
		require(t_pair, it);
}

Rectangle::Rectangle(Pos pos, ExprAST *p1, ExprAST *p2) :
		Object(pos) {
	p1_ = require(t_pair, p1);
	p2_ = require(t_pair, p2);
}

Circle::Circle(Pos pos, ExprAST *c, ExprAST *r) :
		Object(pos) {
	c_ = require(t_pair, c);
	r_ = cast(t_float, r);
}

Arc::Arc(Pos pos, ExprAST *c, ExprAST *r, ExprAST *st, ExprAST *ed) :
		Circle(pos, c, r) {
	st_ = cast(t_float, st);
	ed_ = cast(t_float, ed);
}

Node::Node(Pos pos, OptionList *opt, ExprAST *loc, ExprAST *text, ExprAST *deg) :
		Object(pos), opt_(opt) {
	loc_ = require(t_pair, loc);
	text_ = require(t_string, text);
	deg_ = cast(t_float, deg);
}

ProcDef::ProcDef(Identifier *name, std::vector<Arg> args) :
		AST(*name), name_(name), args_(args), vars_(0) {
	min_args_ = args_.size();
	while (min_args_ > 0 && args_[min_args_ - 1].def)
		min_args_--;
}

FuncDef::FuncDef(Identifier *name, Type ty, std::vector<Arg> args, ExprAST *expr) :
		AST(*name), ty_(ty), name_(name), args_(args), vars_(0) {
	expr_ = cast(ty, expr);
	min_args_ = args_.size();
	while (min_args_ > 0 && args_[min_args_ - 1].def)
		min_args_--;
}

void Store::set(EvalEnv *env, const Value &val) const {
	if (local_)
		env->setLoc(index_, val);
	else
		env->setGlb(index_, val);
}

void ProcCall::castArg(int index, Type ty) {
	list_[index] = cast(ty, list_[index]);
}

void FuncCall::castArg(int index, Type ty) {
	list_[index] = cast(ty, list_[index]);
}

void Program::addProcDef(ProcDef *p) {
	if (procs_.find(p->getName()) != procs_.end()) {
		sprintf(err, "redifinition of procedure %s",
				p->getName().c_str());
		prtError(*p, err);
	}
	procs_.insert(make_pair(p->getName(), p));
}

void Program::addFuncDef(FuncDef *p) {
	if (funcs_.find(p->getName()) != funcs_.end()) {
		sprintf(err, "redifinition of function %s",
				p->getName().c_str());
		prtError(*p, err);
	}
	funcs_.insert(make_pair(p->getName(), p));
}

ProcDef* Program::getProcDef(Identifier *name) const {
	auto v = procs_.find(name->getName());
	if (v == procs_.end()) {
		sprintf(err, "undefined procedure %s", name->getName().c_str());
		prtError(*name, err);
	}
	return v->second;
}

FuncDef* Program::getFuncDef(Identifier *name) const {
	auto v = funcs_.find(name->getName());
	if (v == funcs_.end()) {
		sprintf(err, "undefined function %s", name->getName().c_str());
		prtError(*name, err);
	}
	return v->second;
}

void debug(AST *ast, FILE *f) {
	ast->print(f);
	fprintf(f, "\n");
	fflush(f);
}

void IntExpr::print(FILE *f) const {
	fprintf(f, "(Int %d)", val_);
}

void FloatExpr::print(FILE *f) const {
	fprintf(f, "(Float %.2f)", val_);
}

void OptionStr::print(FILE *f) const {
	fprintf(f, "(OptionStr \"%s\")", str_.c_str());
}

void OptionRef::print(FILE *f) const {
	fprintf(f, "(OptionRef ");
	ref_->print(f);
	fprintf(f, ")");
}

void OptionList::print(FILE *f) const {
	fprintf(f, "(OptionList");
	for (int i = 0; i < list_.size(); i++)
		list_[i]->print(f);
	fprintf(f, ")");
}

void StringExpr::print(FILE *f) const {
	fprintf(f, "(String \"%s\")", str_.c_str());
}

void MultiLine::print(FILE *f) const {
	fprintf(f, "(MultiLine \"%s\"", opt_.c_str());
	for (auto e : list_) {
		fprintf(f, " ");
		e->print(f);
	}
	fprintf(f, ")");
}

void UnaryExpr::print(FILE *f) const {
	fprintf(f, "(Unary \"%s\" ", getTokenStr(op_->type));
	expr_->print(f);
	fprintf(f, ")");
}

void BinIntExpr::print(FILE *f) const {
	fprintf(f, "(Binary ");
	e1_->print(f);
	fprintf(f, " \"%s\" ", getTokenStr(op_->type));
	e2_->print(f);
	fprintf(f, ")");
}

void BinFloatExpr::print(FILE *f) const {
	fprintf(f, "(Binary \"%s\" ", getTokenStr(op_->type));
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void BinStrExpr::print(FILE *f) const {
	fprintf(f, "(Binary \"%s\" ", getTokenStr(op_->type));
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void BinPairExpr::print(FILE *f) const {
	fprintf(f, "(Binary \"%s\" ", getTokenStr(op_->type));
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void ScaleExpr::print(FILE *f) const {
	fprintf(f, "(Scale \"%s\" ", getTokenStr(op_->type));
	p_->print(f);
	fprintf(f, " ");
	c_->print(f);
	fprintf(f, ")");
}

void PairExpr::print(FILE *f) const {
	fprintf(f, "(Pair ");
	fst_->print(f);
	fprintf(f, " ");
	snd_->print(f);
	fprintf(f, ")");
}

void IfExpr::print(FILE *f) const {
	fprintf(f, "(If ");
	cond_->print(f);
	fprintf(f, " ");
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void Store::print(FILE *f) const {
	fprintf(f, "(%s %d)", local_ ? "Local" : "Global", index_);
}

void AsExpr::print(FILE *f) const {
	fprintf(f, "(As ");
	lval_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, "\n");
}

void LetExpr::print(FILE *f) const {
	fprintf(f, "(Let ");
	defs_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, ")");
}

void VarDef::print(FILE *f) const {
	fprintf(f, "(Def ");
	lval_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, ")");
}

void GotoStmt::print(FILE *f) const {
	fprintf(f, "(Goto ");
	loc_->print(f);
	fprintf(f, " ");
	stmt_->print(f);
	fprintf(f, ")");
}

void ScaleStmt::print(FILE *f) const {
	fprintf(f, "(Scale ");
	scale_->print(f);
	fprintf(f, " ");
	stmt_->print(f);
	fprintf(f, ")");
}

void RotateStmt::print(FILE *f) const {
	fprintf(f, "(Rotate ");
	deg_->print(f);
	fprintf(f, " ");
	stmt_->print(f);
	fprintf(f, ")");
}

void WithStmt::print(FILE *f) const {
	fprintf(f, "(With ");
	opts_->print(f);
	fprintf(f, " ");
	stmt_->print(f);
	fprintf(f, ")");
}

void CommentStmt::print(FILE *f) const {
	fprintf(f, "(Comment ");
	stmt_->print(f);
	fprintf(f, ")");
}

void IfStmt::print(FILE *f) const {
	fprintf(f, "(IfStmt ");
	cond_->print(f);
	fprintf(f, " ");
	s1_->print(f);
	if (s2_ != NULL) {
		fprintf(f, " ");
		s2_->print(f);
	}
	fprintf(f, ")");
}

void ForStmt::print(FILE *f) const {
	fprintf(f, "(ForStmt ");
	lval_->print(f);
	fprintf(f, " ");
	st_->print(f);
	fprintf(f, " %s ", inc_ ? "to" : "downto");
	ed_->print(f);
	fprintf(f, " ");
	body_->print(f);
	fprintf(f, ")");
}

void IterStmt::print(FILE *f) const {
	fprintf(f, "(IterStmt ");
	lval_->print(f);
	fprintf(f, " (ExprList");
	for (ExprAST *it : list_) {
		fprintf(f, " ");
		it->print(f);
	}
	fprintf(f, ") ");
	body_->print(f);
	fprintf(f, ")");
}

void Line::print(FILE *f) const {
	fprintf(f, "(Line");
	for (auto e : list_) {
		fprintf(f, " ");
		e->print(f);
	}
	fprintf(f, ")");
}

void Cycle::print(FILE *f) const {
	fprintf(f, "(Cycle");
	for (auto e : list_) {
		fprintf(f, " ");
		e->print(f);
	}
	fprintf(f, ")");
}

void Rectangle::print(FILE *f) const {
	fprintf(f, "(Rect ");
		if (p1_ == NULL)
			prtError(*this, "rect!!");
	p1_->print(f);
	fprintf(f, " ");
		if (p2_ == NULL)
			prtError(*this, "rect!!");
	p2_->print(f);
	fprintf(f, ")");
}

void Circle::print(FILE *f) const {
	fprintf(f, "(Circle ");
	c_->print(f);
	fprintf(f, " ");
	r_->print(f);
	fprintf(f, ")");
}

void Arc::print(FILE *f) const {
	fprintf(f, "(Arc ");
	c_->print(f);
	fprintf(f, " ");
	r_->print(f);
	fprintf(f, " ");
	st_->print(f);
	fprintf(f, " ");
	ed_->print(f);
	fprintf(f, ")");
}

void Node::print(FILE *f) const {
	fprintf(f, "(Node ");
	if (opt_ != NULL) {
		opt_->print(f);
		fprintf(f, " ");
	}
	loc_->print(f);
	fprintf(f, " ");
	text_->print(f);
	fprintf(f, ")");
}

void DrawStmt::print(FILE *f) const {
	fprintf(f, "(Draw ");
	if (opt_ != NULL) {
		opt_->print(f);
		fprintf(f, " ");
	}
	obj_->print(f);
	fprintf(f, ")");
	fflush(f);
}

void StmtSeq::print(FILE *f) const {
	fprintf(f, "(StmtSeq");
	for (auto s : list_) {
		fprintf(f, " ");
		s->print(f);
	}
	fprintf(f, ")");
	fflush(f);
}

void ProcDef::print(FILE *f) const {
	fprintf(f, "(Proc %s ", name_->getName().c_str());
	for (auto arg : args_) {
		fprintf(f, "(Arg ");
		arg.store->print(f);
		fprintf(f, " %s) ", getTypeStr(arg.ty));
	}
	fprintf(f, "(List");
	for (auto s : stmts_) {
		fprintf(f, " ");
		s->print(f);
	}
	fprintf(f, "))");
}

void FuncDef::print(FILE *f) const {
	fprintf(f, "(Func %s ", name_->getName().c_str());
	for (auto arg : args_) {
		fprintf(f, "(Arg ");
		arg.store->print(f);
		fprintf(f, " %s) ", getTypeStr(arg.ty));
	}
	expr_->print(f);
	fprintf(f, ")");
}

void ProcCall::print(FILE *f) const {
	fprintf(f, "(Call %s", name_->getName().c_str());
	for (auto e : list_) {
		fprintf(f, " ");
		e->print(f);
	}
	fprintf(f, ")");
}

void FuncCall::print(FILE *f) const {
	fprintf(f, "(Call %s", name_->getName().c_str());
	for (auto e : list_) {
		fprintf(f, " ");
		e->print(f);
	}
	fprintf(f, ")");
}

void Program::print(FILE *f) const {
	fprintf(f, "(Program");
	if (opt_ != NULL) {
		fprintf(f, " ");
		opt_->print(f);
	}
	for (auto p : funcs_) {
		fprintf(f, " ");
		p.second->print(f);
	}
	for (auto p : procs_) {
		fprintf(f, " ");
		p.second->print(f);
	}
	fprintf(f, ")");
}


Value IntExpr::eval(EvalEnv *env) const {
	return ivalue(val_);
}

Value FloatExpr::eval(EvalEnv *env) const {
	return fvalue(val_);
}

Value StringExpr::eval(EvalEnv *env) const {
	return svalue(str_.c_str());
}

Value MultiLine::eval(EvalEnv *env) const {
	string sep = "\\\\", ret;
	if (opt_.size() > 0)
		sep += string("[") + opt_ + string("]");
	ret = list_[0]->eval(env).s;
	for (int i = 1; i < list_.size(); i++)
		ret += sep + list_[i]->eval(env).s;
	return svalue(strdup(ret.c_str())); // ret is released!!
}

Value UnaryExpr::eval(EvalEnv *env) const {
	int opr = op_->type;
	Value r = expr_->eval(env);
	if (opr == tok_float)
		return fvalue(r.i);
	if (opr == tok_int)
		return ivalue((int) r.f1);
	if (opr == tok_sub)
		return Value(-r.i, -r.f1, -r.f2, "");
	if (opr == tok_not)
		return ivalue(!r.i);
	if (opr == tok_abs)
		return ivalue(abs(r.i));
	if (opr == tok_fst)
		return fvalue(r.f1);
	if (opr == tok_snd)
		return fvalue(r.f2);
	double dv = r.f1;
	if (opr == tok_fabs)
		return fvalue(fabs(dv));
	if (opr == tok_sin)
		return fvalue(sin(dv * cvt));
	if (opr == tok_cos)
		return fvalue(cos(dv * cvt));
	if (opr == tok_tan)
		return fvalue(tan(dv * cvt));
	if (opr == tok_sqrt)
		return fvalue(sqrt(dv));
	if (opr == tok_str)
		return svalue(strdup(getFloatStr(dv)));
	fprintf(stderr, "line %d.%d: op = %s\n", row, col, getTokenStr(opr));
	t_assert(false, "UnaryExpr::eval(env) -- should not reach");
	return Value();
}

Value BinIntExpr::eval(EvalEnv *env) const {
	int opr = op_->type;
	int v1 = e1_->eval(env).i;
	int v2 = e2_->eval(env).i;
	if (opr >= tok_add && opr <= tok_mod) {
		switch (opr) {
		case tok_add: return ivalue(v1 + v2);
		case tok_sub: return ivalue(v1 - v2);
		case tok_mul: return ivalue(v1 * v2);
		case tok_div:
			if (v2 == 0)
				prtError(*e2_, "division by zero");
			return ivalue(v1 / v2);
		case tok_mod: return ivalue(v1 % v2);
		}
	}
	if (opr >= tok_lt && opr <= tok_ne)
		switch (opr) {
		case tok_lt: return ivalue(v1 <  v2);
		case tok_le: return ivalue(v1 <= v2);
		case tok_gt: return ivalue(v1 >  v2);
		case tok_ge: return ivalue(v1 >= v2);
		case tok_eq: return ivalue(v1 == v2);
		case tok_ne: return ivalue(v1 != v2);
		}
	switch (opr) {
	case tok_and: return ivalue(v1 && v2);
	case tok_or : return ivalue(v1 || v2);
	case tok_shl: return ivalue(v1 << v2);
	case tok_shr: return ivalue(v1 >> v2);
	case tok_max: return ivalue(v1 > v2 ? v1 : v2);
	case tok_min: return ivalue(v1 < v2 ? v1 : v2);
	}
	fprintf(stderr, "line %d.%d: op = %s\n", row, col, getTokenStr(opr));
	t_assert(false, "BinIntExpr::eval(env) -- should not reach");
	return Value();
}

Value BinFloatExpr::eval(EvalEnv *env) const {
	int opr = op_->type;
	double v1 = e1_->eval(env).f1;
	double v2 = e2_->eval(env).f1;
	if (opr >= tok_add && opr <= tok_div) {
		switch (opr) {
		case tok_add: return fvalue(v1 + v2);
		case tok_sub: return fvalue(v1 - v2);
		case tok_mul: return fvalue(v1 * v2);
		case tok_div:
			if (fabs(v2) < 1e-9)
				prtError(*e2_, "division by zero");
			return fvalue(v1 / v2);
		}
	}
	if (opr >= tok_lt && opr <= tok_ne)
		switch (opr) {
		case tok_lt: return ivalue(v1 <  v2);
		case tok_le: return ivalue(v1 <= v2);
		case tok_gt: return ivalue(v1 >  v2);
		case tok_ge: return ivalue(v1 >= v2);
		case tok_eq: return ivalue(v1 == v2);
		case tok_ne: return ivalue(v1 != v2);
		}
	if (opr == tok_atan2)
		return fvalue(atan2(v1, v2) / cvt);
	fprintf(stderr, "line %d.%d: op = %s\n", row, col, getTokenStr(opr));
	t_assert(false, "BinFloatExpr::eval(env) -- should not reach");
	return Value();
}

Value BinStrExpr::eval(EvalEnv *env) const {
	int opr = op_->type;
	const char *s1 = e1_->eval(env).s;
	const char *s2 = e2_->eval(env).s;
	if (opr == tok_concat) {
		int l1 = strlen(s1), l2 = strlen(s2);
		char *buf = new char[l1 + l2 + 2];
		strcpy(buf, s1);
		strcat(buf, s2);
		return svalue(buf);
	}
	if (opr == tok_eq)
		return ivalue(!strcmp(s1, s2));
	if (opr == tok_ne)
		return ivalue(strcmp(s1, s2));
	fprintf(stderr, "line %d.%d: op = %s\n", row, col, getTokenStr(opr));
	t_assert(false, "BinStrExpr::eval(env) -- should not reach");
	return Value();
}

Value BinPairExpr::eval(EvalEnv *env) const {
	int opr = op_->type;
	Value v1 = e1_->eval(env);
	Value v2 = e2_->eval(env);
	if (opr == tok_add)
		return pvalue(v1.f1 + v2.f1, v1.f2 + v2.f2);
	if (opr == tok_sub)
		return pvalue(v1.f1 - v2.f1, v1.f2 - v2.f2);
	if (opr == tok_dis) {
		double dx = v1.f1 - v2.f1, dy = v1.f2 - v2.f2;
		return fvalue(sqrt(dx * dx + dy * dy));
	}
	fprintf(stderr, "line %d.%d: op = %s\n", row, col, getTokenStr(opr));
	t_assert(false, "BinPairExpr::eval(env) -- should not reach");
	return Value();
}

Value ScaleExpr::eval(EvalEnv *env) const {
	Value p = p_->eval(env);
	if (op_->type == tok_sub)
		return pvalue(-p.f1, -p.f2);
	double scale = c_->eval(env).f1;
	if (op_->type == tok_mul)
		return pvalue(p.f1*scale, p.f2*scale);
	if (op_->type == tok_div)
		return pvalue(p.f1/scale, p.f2/scale);
	fprintf(stderr, "line %d.%d: op = %s\n", row, col, getTokenStr(op_->type));
	t_assert(false, "ScaleExpr::eval(env) -- should not reach");
	return Value();
}

Value PairExpr::eval(EvalEnv *env) const {
	double v1 = fst_->eval(env).f1;
	double v2 = snd_->eval(env).f1;
	return pvalue(v1, v2);
}

Value IfExpr::eval(EvalEnv *env) const {
	int c = cond_->eval(env).i;
	return (c ? e1_->eval(env) : e2_->eval(env));
}

Value Store::eval(EvalEnv *env) const {
	return (local_ ? env->getLoc(index_) : env->getGlb(index_));
}

Value AsExpr::eval(EvalEnv *env) const {
	Value ret = expr_->eval(env);
	lval_->set(env, ret);
	return ret;
}

Value LetExpr::eval(EvalEnv *env) const {
	defs_->exec(env, NULL);
	return expr_->eval(env);
}

Value FuncCall::eval(EvalEnv *env) const {
	ValueMap *gmap = env->getGlbMap();
	ValueMap *lmap = new ValueMap(ref_->getVars(), ref_->getArgs());
	for (int i = 0; i < list_.size(); i++)
		lmap->set(i, list_[i]->eval(env));
	EvalEnv *sub = new EvalEnv(gmap, lmap);
	Value ret = ref_->eval(sub);
	delete lmap;
	delete sub;
	return ret;
}

ExprAST* unary(Operator *op, ExprAST *e) {
	int opr = op->type;
	Type ty = e->type();
	if (ty == t_string)
		prtTypeError(*op, "require numeric value");
	if (opr == tok_sub) {
		if (ty == t_pair)
			return new ScaleExpr(op, e, new FloatExpr(Pos(), 0));
		return new UnaryExpr(op, e, ty);
	}
	if (opr == tok_not || opr == tok_abs)
		return new UnaryExpr(op, require(t_int, e), t_int);
	if (opr == tok_fst || opr == tok_snd)
		return new UnaryExpr(op, require(t_pair, e), t_float);
	if (opr == tok_fabs || opr == tok_sin  || opr == tok_cos ||
		opr == tok_tan  || opr == tok_sqrt)
		return new UnaryExpr(op, cast(t_float, e), t_float);
	if (opr == tok_int)
		return new UnaryExpr(op, require(t_float, e), t_int);
	if (opr == tok_float)
		return new UnaryExpr(op, require(t_int, e), t_float);
	if (opr == tok_str)
		return new UnaryExpr(op, cast(t_float, e), t_string);
	t_assert(false, "unary(op, e) -- should not reach");
	return NULL;
}

ExprAST* binary(Operator *op, ExprAST *e1, ExprAST *e2) {
	int opr = op->type;
	Type t1 = e1->type(), t2 = e2->type(), mxt = max(t1, t2);
	if (mxt == t_string) {
		if (opr == tok_eq || opr == tok_ne) {
			if (t1 != t_string || t2 != t_string)
				prtBinOpError(op, "type mismatch");
			return new BinStrExpr(op, e1, e2, t_int);
		}
		if (opr != tok_concat)
			prtBinOpError(op, "type mismatch");
		if (t1 != t_string || t2 != t_string)
			prtBinOpError(op, "require string on both operands");
		return new BinStrExpr(op, e1, e2, t_string);
	}
	if (opr == tok_eq || opr == tok_ne || opr == tok_lt || opr == tok_le ||
		opr == tok_gt || opr == tok_ge) {
		if (mxt == t_int)
			return new BinIntExpr(op, e1, e2, t_int);
		else
			return new BinFloatExpr(op, cast(t_float, e1),
					cast(t_float, e2), t_int);
		prtBinOpError(op, "type mismatch");
	}
	if (opr == tok_and || opr == tok_or || opr == tok_mod ||
		opr == tok_shl || opr == tok_shr) {
		if (t1 != t_int || t2 != t_int)
			prtBinOpError(op, "require int on both operands");
		return new BinIntExpr(op, e1, e2, t_int);
	}
	if (opr == tok_atan2) {
		if (mxt > t_float)
			prtBinOpError(op, "require float on both arguments");
		return new BinFloatExpr(op, cast(t_float, e1),
				cast(t_float, e2), t_float);
	}
	if (opr == tok_dis) {
		if (t1 != t_pair || t2 != t_pair)
			prtBinOpError(op, "require pair on both arguments");
		return new BinPairExpr(op, e1, e2, t_float);
	}
	if (opr == tok_add || opr == tok_sub) {
		if (t1 == t_pair && t2 == t_pair)
			return new BinPairExpr(op, e1, e2, t_pair);
		if (mxt == t_int)
			return new BinIntExpr(op, e1, e2, t_int);
		if (mxt == t_float)
			return new BinFloatExpr(op, cast(t_float, e1),
					cast(t_float, e2), t_float);
		prtBinOpError(op, "type mismatch");
	}
	if (opr == tok_mul || opr == tok_div) {
		if (t1 == t_pair && t2 == t_pair) {
			sprintf(err, "cannot perform '%s' on pairs", getTokenStr(opr));
			prtTypeError(*op, err);
		}
		if (mxt == t_int)
			return new BinIntExpr(op, e1, e2, t_int);
		if (mxt == t_float)
			return new BinFloatExpr(op, cast(t_float, e1),
					cast(t_float, e2), t_float);
		if (t1 == t_pair)
			return new ScaleExpr(op, e1, cast(t_float, e2));
		if (t2 == t_pair && opr == tok_mul)
			return new ScaleExpr(op, e2, cast(t_float, e1));
		prtBinOpError(op, "type mismatch");
	}
	t_assert(false, "binary(op, e1, e2) -- should not reach");
	return NULL;
}

void VarDef::exec(EvalEnv *env, FILE *f) const {
	lval_->set(env, expr_->eval(env));
}

void StmtSeq::exec(EvalEnv *env, FILE *f) const {
	for (auto stmt : list_)
		stmt->exec(env, f);
}

void ProcCall::exec(EvalEnv *env, FILE *f) const {
	ValueMap *gmap = env->getGlbMap();
	ValueMap *lmap = new ValueMap(ref_->getVars(), ref_->getArgs());
	for (int i = 0; i < list_.size(); i++)
		lmap->set(i, list_[i]->eval(env));
	EvalEnv *sub = new EvalEnv(gmap, lmap);
	ref_->execute(sub, f);
	delete lmap;
	delete sub;
}

void GotoStmt::exec(EvalEnv *env, FILE *f) const {
	Value val = loc_->eval(env);
	trans->move(val.f1, val.f2);
	stmt_->exec(env, f);
	trans->move(-val.f1, -val.f2);
}

void ScaleStmt::exec(EvalEnv *env, FILE *f) const {
	double s = scale_->eval(env).f1;
	if (fabs(s) > 1e-9) {
		trans->scale(s);
		stmt_->exec(env, f);
		trans->scale(1.0 / s);
	}
}

void RotateStmt::exec(EvalEnv *env, FILE *f) const {
	double d = deg_->eval(env).f1;
	trans->rotate(d);
	stmt_->exec(env, f);
	trans->rotate(-d);
}

void WithStmt::exec(EvalEnv *env, FILE *f) const {
	env->pushOptionList(opts_);
	stmt_->exec(env, f);
	env->popOptionList();
}

void CommentStmt::exec(EvalEnv *env, FILE *f) const {
	if (!mask_)
		stmt_->exec(env, f);
}

void IfStmt::exec(EvalEnv *env, FILE *f) const {
	int c = cond_->eval(env).i;
	if (c)
		s1_->exec(env, f);
	if (!c && s2_ != NULL)
		s2_->exec(env, f);
}

void ForStmt::exec(EvalEnv *env, FILE *f) const {
	int st = st_->eval(env).i;
	int ed = ed_->eval(env).i;
	if (inc_)
		for (int i = st; i <= ed; i++) {
			lval_->set(env, ivalue(i));
			body_->exec(env, f);
		}
	else {
		for (int i = st; i >= ed; i--) {
			lval_->set(env, ivalue(i));
			body_->exec(env, f);
		}
	}
}

void IterStmt::exec(EvalEnv *env, FILE *f) const {
	for (ExprAST *it : list_) {
		lval_->set(env, it->eval(env));
		body_->exec(env, f);
	}
}

void DrawStmt::exec(EvalEnv *env, FILE *f) const {
	fprintf(f, "\\draw ");
	env->printOptionList(opt_, f);
	obj_->dump(env, f);
	fprintf(f, ";\n");
}

void ProcDef::execute(EvalEnv *env, FILE *f) const {
	for (auto stmt : stmts_)
		stmt->exec(env, f);
}

Value FuncDef::eval(EvalEnv *env) const {
	return expr_->eval(env);
}

void Program::execute(FILE *f) const {
	ProcDef *main = getProcDef(new Identifier(Pos(), string("_main_")));
	ValueMap *gmap = new ValueMap(vars_);
	ValueMap *lmap = new ValueMap(main->getVars());
	EvalEnv *env = new EvalEnv(gmap, lmap);
	fprintf(f, "\\documentclass[border=1pt]{standalone}\n");
	fprintf(f, "\\usepackage[dvipsnames]{xcolor}\n");
 	fprintf(f, "\\usepackage{tikz}\n");
 	for (auto &hd : header_)
 		fprintf(f, "%s\n", hd.c_str());
	fprintf(f, "\\begin{document}\n");
	fprintf(f, "\\begin{tikzpicture}");
	if (opt_ != NULL)
		env->printOptionList(opt_, f);
	fprintf(f, "\n");
	main->execute(env, f);
	fprintf(f, "\\end{tikzpicture}\n");
	fprintf(f, "\\end{document}\n");
	delete lmap;
	delete gmap;
	delete env;
}

void OptionStr::list(EvalEnv *env, FILE *f) const {
	fprintf(f, "%s", str_.c_str());
}

void OptionRef::list(EvalEnv *env, FILE *f) const {
	Value v = ref_->eval(env);
	fprintf(f, "%s", v.s);
}

bool OptionList::list(EvalEnv *env, bool comma, FILE *f) {
	if (list_.empty()) return comma;
	for (int i = 0; i < list_.size(); i++) {
		if (comma) fprintf(f, ",");
		list_[i]->list(env, f);
		comma = true;
	}
	return true;
}

void Line::dump(EvalEnv *env, FILE *f) const {
	for (int i = 0; i < list_.size(); i++) {
		if (i > 0) fprintf(f, " -- ");
		Value p = list_[i]->eval(env);
		trans->prtPair(p.f1, p.f2, f);
	}
}

void Cycle::dump(EvalEnv *env, FILE *f) const {
	for (int i = 0; i < list_.size(); i++) {
		Value p = list_[i]->eval(env);
		trans->prtPair(p.f1, p.f2, f);
		fprintf(f, " -- ");
	}
	fprintf(f, "cycle");
}

void Rectangle::dump(EvalEnv *env, FILE *f) const {
	Value p1 = p1_->eval(env);
	Value p2 = p2_->eval(env);
	trans->prtPair(p1.f1, p1.f2, f);
	fprintf(f, " rectangle ");
	trans->prtPair(p2.f1, p2.f2, f);
}

void Circle::dump(EvalEnv *env, FILE *f) const {
	Value c = c_->eval(env);
	double r = r_->eval(env).f1;
	trans->prtPair(c.f1, c.f2, f);
	fprintf(f, " circle (");
	trans->prtLength(r, f);
	fprintf(f, ")");
}

void Arc::dump(EvalEnv *env, FILE *f) const {
	Value   c = c_->eval(env);
	double st = st_->eval(env).f1;
	double ed = ed_->eval(env).f1;
	double ra = trans->length(r_->eval(env).f1);
	double px = c.f1 + ra * cos(st * cvt);
	double py = c.f2 + ra * sin(st * cvt);
	trans->prtPair(px, py, f);
	fprintf(f, " arc (");
	prettyFloat(st, f);
	fprintf(f, ":");
	prettyFloat(ed, f);
	fprintf(f, ":");
	prettyFloat(ra, f);
	fprintf(f, ")");
}

void Node::dump(EvalEnv *env, FILE *f) const {
	//static char ss[64];
	Value c = loc_->eval(env);
	trans->prtPair(c.f1, c.f2, f);
	fprintf(f, " node ");
	double deg = trans->curr_degree() + deg_->eval(env).f1;
	if (fabs(deg) > 1e-8)
		fprintf(f, "[rotate=%.2f]", deg);
	if (opt_ != NULL)
		env->printOptionList(opt_, f);
	fprintf(f, "{%s}", text_->eval(env).s);
}
