#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <cstdio>
#include <cstdlib>

#include "errorinfo.h"

enum TokenID {
	tok_NULL,
	tok_eof = 1,
	// basic
	tok_comm,
	tok_ival,
	tok_fval,
	tok_sval,
	tok_spec,
	tok_head,
	tok_name,
	tok_link,
	// symbols
	tok_lbracket,
	tok_rbracket,
	tok_lparen,
	tok_rparen,
	tok_lbrace,
	tok_rbrace,
	tok_comma,
	tok_conn, // --
	tok_colon,
	tok_semicol,
	tok_ques,
	tok_bind, // =
	// operators
	tok_add,
	tok_sub,
	tok_mul,
	tok_div,
	tok_mod,
	tok_lt,
	tok_le,
	tok_gt,
	tok_ge,
	tok_eq,
	tok_ne,
	tok_and,
	tok_or,
	tok_shl,
	tok_shr,
	tok_not,
	// keywords
	tok_tikz,
	tok_def,
	tok_fn,
	tok_var,
	tok_int,
	tok_float,
	tok_pair,
	tok_string,
	tok_option,
	tok_anchor,
	tok_rotate,
	tok_scale,
	tok_with,
	tok_comment,
	tok_if,
	tok_else,
	tok_for,
	tok_to,
	tok_downto,
	tok_do,
	tok_let,
	tok_in,
	tok_draw,
	tok_line,
	tok_cycle,
	tok_rect,
	tok_circle,
	tok_radius,
	tok_arc,
	tok_range,
	tok_node,
	tok_at,
	tok_text,
	tok_as,
	tok_multi,
	tok_true,
	tok_false,
	// buildins
	tok_abs,
	tok_fabs,
	tok_sin,
	tok_cos,
	tok_tan,
	tok_atan2,
	tok_sqrt,
	tok_dis,
	tok_fst,
	tok_snd,
	tok_max,
	tok_min,
	tok_str,
	tok_concat,
	// end
	tok_ALL
};

struct Token : Pos {
	int type;
	const char *val;

	Token():Pos(),type(0),val(NULL) {}
	Token(int row, int col, int type, const char *val = NULL) :
			Pos(row, col), type(type), val(val) {}
	Token(const Token &tok) :
			Pos(tok), type(tok.type), val(tok.val) {}
};

void setInputStream(FILE *f);
void initTokenizer();

Token getNextToken(bool comm = false);
const char *getTokenStr(Token tok);
const char *getTokenStr(int type);

#endif
