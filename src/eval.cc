#include "ast.h"
#include "errorinfo.h"
#include "eval.h"
#include "types.h"

#include <cmath>
#include <cstdio>
#include <cstring>
#include <set>

using namespace std;

const char* getValueStr(const Value &val, Type ty) {
	static char f[32];
	switch (ty) {
	case t_int:
		sprintf(f, "[%d]", val.i);
		break;
	case t_float:
		sprintf(f, "[%.2f]", val.f1);
		break;
	case t_pair:
		sprintf(f, "[%.2f, %.2f]", val.f1, val.f2);
		break;
	case t_string:
		sprintf(f, "[%s]", val.s);
		break;
	case t_unit:
		sprintf(f, "[unit]");
		break;
	}
	return f;
}

const char* getFloatStr(double v) {
	static char buf[32];
	const double eps = 1e-8;
	int x, sgn = (v < -eps ? -1 : v > eps);
	char *s = buf, *c = buf + 31;
	if (v < 0) v = -v;
	v += eps;
	x = (int) v;
	v -= x;
	*c = 0;
	do {
		*--c = '0' + (x % 10);
		x /= 10;
	} while (x > 0);
	if (sgn == -1) *s++ = '-';
	while (*c) *s++ = *c++;
	double thr = 1e-7;
	if (v > thr) {
		*s++ = '.';
		for (int i = 0; i < 6; i++) {
			v *= 10;
			x = (int) v;
			v -= x;
			*s++ = '0' + x;
			thr *= 10;
			if (v < thr) break;
		}
	}
	*s = 0;
	return buf;
}

void prettyFloat(double v, FILE *f) {
	fprintf(f, "%s", getFloatStr(v));
}

void prettyPair(double x, double y, FILE *f) {
	fprintf(f, "(");
	prettyFloat(x, f);
	fprintf(f, ", ");
	prettyFloat(y, f);
	fprintf(f, ")");
}

ValueMap::ValueMap(int num) : num_(num) {
	vals_ = new Value[num];
}

ValueMap::ValueMap(int num, const vector<Arg> &args) : num_(num) {
	vals_ = new Value[num];
	for (int i = 0; i < args.size(); i++)
		if (args[i].def)
			vals_[i] = args[i].val;
}

ValueMap::~ValueMap() {
	delete vals_;
}

Value ValueMap::get(int index) const {
	t_assert(0 <= index && index < num_, "Value::get(): index out of range");
	return vals_[index];
}

void ValueMap::set(int index, const Value &val) {
	t_assert(0 <= index && index < num_, "Value::set(): index out of range");
	vals_[index] = val;
}

void EvalEnv::pushOptionList(OptionList *opts) {
	opts_.push_back(opts);
}

void EvalEnv::popOptionList() {
	opts_.pop_back();
}

void EvalEnv::printOptionList(OptionList *opts, FILE *f) {
	if (opts_.empty() && opts == NULL) return;
	opts_.push_back(opts);
	fprintf(f, "[");
	bool comma = false;
	for (int i = 0; i < opts_.size(); i++)
		if (opts_[i] != NULL)
			comma = opts_[i]->list(this, comma, f);
	fprintf(f, "] ");
	opts_.pop_back();
}

void TransSystem::move(double x, double y) {
	w[2] += x * w[0] + y * w[1];
	w[5] += x * w[3] + y * w[4];
}

void TransSystem::scale(double s) {
	w[0] *= s;
	w[1] *= s;
	w[3] *= s;
	w[4] *= s;
	sca *= s;
}

void TransSystem::rotate(double d) {
	double c = cos(d * cvt);
	double s = sin(d * cvt);
	double *u = w + 6;
	u[0] = c * w[0] + s * w[1];
	u[3] = c * w[3] + s * w[4];
	u[1] = c * w[1] - s * w[0];
	u[4] = c * w[4] - s * w[3];
	u[2] = w[2];
	u[5] = w[5];
	memcpy(w, u, sizeof(double) * 6);
	deg += d;
}

void TransSystem::prtLength(double l, FILE *f) {
	prettyFloat(l * sqrt(w[0] * w[0] + w[1] * w[1]), f);
}

void TransSystem::prtPair(double x, double y, FILE *f) {
	double xx = w[0] * x + w[1] * y + w[2];
	double yy = w[3] * x + w[4] * y + w[5];
	prettyPair(xx, yy, f);
}

double TransSystem::length(double l) {
	return l * sqrt(w[0] * w[0] + w[1] * w[1]);
}

double TransSystem::curr_degree() const {
	return deg;
}

double TransSystem::curr_scale() const {
	return sca;
}
