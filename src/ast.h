#ifndef _AST_H
#define _AST_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
#include <stack>
#include <string>
#include <vector>

#include "errorinfo.h"
#include "eval.h"
#include "tokenizer.h"
#include "types.h"

enum TrType { TR_MOVE, TR_SCALE, TR_ROTATE };
enum NodeID {
	i_int,  i_float,  i_pair,  i_string,
	i_bint, i_bfloat, i_bpair, i_bstr,   i_scale,
	i_if,   i_unary,  i_store, i_let,
	i_call, i_multi,  i_as
};

class AST : public Pos {
public:
	AST(Pos pos) : Pos(pos) {}

	virtual void print(FILE *f) const = 0;
};

class Operator : public Pos {
public:
	int type;
	Operator(Pos pos, int type) : Pos(pos), type(type) {}
	Operator(int type) : Pos(), type(type) {}
};

class ExprAST : public AST {
protected:
	NodeID node_;
	Type type_;
public:
	ExprAST(Pos pos, Type type, NodeID node):
			AST(pos), type_(type), node_(node) {}

	virtual NodeID id() const { return node_; }
	virtual Type type() const { return type_; }
	virtual void print(FILE *f) const = 0;
	virtual Value eval(EvalEnv *env) const = 0;
};

class IntExpr : public ExprAST {
protected:
	int val_;
public:
	IntExpr(Pos pos, int val) :
			ExprAST(pos, t_int, i_int), val_(val) {}
	
	int getval() { return val_; }
	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class FloatExpr : public ExprAST {
protected:
	double val_;
public:
	FloatExpr(Pos pos, double val) :
			ExprAST(pos, t_float, i_float), val_(val) {}

	double getval() { return val_; }
	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class Option : public AST {
public:
	Option(Pos pos) : AST(pos) {}
	
	virtual void print(FILE *f) const = 0;
	virtual void list(EvalEnv *env, FILE *f) const = 0;
};

class OptionStr : public Option {
protected:
	std::string str_;
public:
	OptionStr(Pos pos, std::string str) : Option(pos), str_(str) {}

	std::string getStr() { return str_; }
	void print(FILE *f) const;
	void list(EvalEnv *env, FILE *f) const;
};

class Store;
class OptionRef : public Option {
protected:
	Store *ref_;
public:
	OptionRef(Pos pos, Store *ref);

	void print(FILE *f) const;
	void list(EvalEnv *env, FILE *f) const;
};

class OptionList : public AST {
protected:
	std::vector<Option*> list_;
public:
	OptionList(Pos pos) : AST(pos) {}
	OptionList(Pos pos, std::vector<Option*> list) :
			AST(pos), list_(list) {}

	void add(Option *opt) { list_.push_back(opt); }
	Option* get(int index) { return list_[index]; }
	size_t size() const { return list_.size(); }
	void print(FILE *f) const;
	bool list(EvalEnv *env, bool comma, FILE *f);
};

class StringExpr : public ExprAST {
protected:
	std::string str_;
public:
	StringExpr(Pos pos, std::string str) :
			ExprAST(pos, t_string, i_string), str_(str) {}

	std::string getval() { return str_; }
	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class MultiLine : public ExprAST {
protected:
	std::string opt_;
	std::vector<ExprAST*> list_;
public:
	MultiLine(Pos pos, std::string str, std::vector<ExprAST*> list);

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class UnaryExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *expr_;
public:
	UnaryExpr(Operator *op, ExprAST *expr, Type ty) :
			ExprAST(*op, ty, i_unary), op_(op), expr_(expr) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class BinIntExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *e1_, *e2_; // int and int
public:
	BinIntExpr(Operator *op, ExprAST *e1, ExprAST *e2, Type ty) :
			ExprAST(*op, ty, i_bint), op_(op), e1_(e1), e2_(e2) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class BinFloatExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *e1_, *e2_; // float and float
public:
	BinFloatExpr(Operator *op, ExprAST *e1, ExprAST *e2, Type ty) :
			ExprAST(*op, ty, i_bfloat), op_(op), e1_(e1), e2_(e2) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class BinPairExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *e1_, *e2_; // pair and pair
public:
	BinPairExpr(Operator *op, ExprAST *e1, ExprAST *e2, Type ty) :
			ExprAST(*op, ty, i_bpair), op_(op), e1_(e1), e2_(e2) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class BinStrExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *e1_, *e2_; // string and string
public:
	BinStrExpr(Operator *op, ExprAST *e1, ExprAST *e2, Type ty) :
			ExprAST(*op, ty, i_bstr), op_(op), e1_(e1), e2_(e2) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class ScaleExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *p_, *c_; // pair and float
public:
	ScaleExpr(Operator *op, ExprAST *p, ExprAST *c, Type ty = t_pair) :
			ExprAST(*op, ty, i_scale), op_(op), p_(p), c_(c) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class PairExpr : public ExprAST {
protected:
	ExprAST *fst_, *snd_;
public:
	PairExpr(Pos pos, ExprAST *fst, ExprAST *snd);
	
	ExprAST* fst() { return fst_; }
	ExprAST* snd() { return snd_; }
	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class IfExpr : public ExprAST {
protected:
	ExprAST *cond_, *e1_, *e2_;
public:
	IfExpr(Pos pos, ExprAST *cond_, ExprAST *e1, ExprAST *e2);

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class Identifier : public Pos {
private:
	std::string name_;
public:
	Identifier(Pos pos, std::string name) :
			Pos(pos), name_(name) {}

	std::string getName() const { return name_; }
};

class Store : public ExprAST {
private:
	int local_, index_;
public:
	Store(Pos pos, Type t, int local, int index) :
			ExprAST(pos, t, i_store), local_(local), index_(index) {}

	int local() const { return local_; }
	int index() const { return index_; }
	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
	void set(EvalEnv *env, const Value &val) const;
};

class AsExpr : public ExprAST {
protected:
	Store *lval_;
	ExprAST *expr_;
public:
	AsExpr(Pos pos, Store *lval, ExprAST *expr) :
			ExprAST(pos, expr->type(), i_as), lval_(lval), expr_(expr) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class StmtAST : public AST {
public:
	StmtAST(Pos pos): AST(pos) {}

	virtual void print(FILE *f) const = 0;
	virtual void exec(EvalEnv *env, FILE *f) const {};
};

class LetExpr : public ExprAST {
private:
	StmtAST *defs_;
	ExprAST *expr_;
public:
	LetExpr(Pos pos, StmtAST *defs, ExprAST *expr) :
			ExprAST(pos, expr->type(), i_let),
			defs_(defs), expr_(expr) {}

	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class VarDef : public StmtAST {
protected:
	Store *lval_;
	ExprAST *expr_;
public:
	VarDef(Store *lval, ExprAST *expr) :
			StmtAST(*lval), lval_(lval), expr_(expr) {}

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class GotoStmt : public StmtAST {
protected:
	ExprAST *loc_;
	StmtAST *stmt_;
public:
	GotoStmt(Pos pos, ExprAST *loc, StmtAST *stmt);

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class ScaleStmt : public StmtAST {
protected:
	ExprAST *scale_;
	StmtAST *stmt_;
public:
	ScaleStmt(Pos pos, ExprAST *scale, StmtAST *stmt);

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class RotateStmt : public StmtAST {
protected:
	ExprAST *deg_;
	StmtAST *stmt_;
public:
	RotateStmt(Pos pos, ExprAST *deg, StmtAST *stmt);

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class WithStmt : public StmtAST {
protected:
	OptionList *opts_;
	StmtAST *stmt_;
public:
	WithStmt(Pos pos, OptionList *opts, StmtAST *stmt) :
			StmtAST(pos), opts_(opts), stmt_(stmt) {}

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class CommentStmt : public StmtAST {
protected:
	bool mask_;
	StmtAST *stmt_;
public:
	CommentStmt(Pos pos, bool mask, StmtAST *stmt) :
			StmtAST(pos), mask_(mask), stmt_(stmt) {}

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class IfStmt : public StmtAST {
protected:
	ExprAST *cond_;
	StmtAST *s1_, *s2_;
public:
	IfStmt(Pos pos, ExprAST *cond, StmtAST *s1, StmtAST *s2);

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class ForStmt : public StmtAST {
protected:
	Store *lval_;
	ExprAST *st_, *ed_;
	int inc_;
	StmtAST *body_;
public:
	ForStmt(Pos pos, Store *lval, ExprAST *st,
			ExprAST *ed, int inc, StmtAST *body);

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class IterStmt : public StmtAST {
protected:
	Store *lval_;
	std::vector<ExprAST*> list_;
	StmtAST *body_;
public:
	IterStmt(Pos pos, Store *lval, std::vector<ExprAST*> list, StmtAST *body);

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class Object : public AST {
protected:
public:
	Object(Pos pos) : AST(pos) {}

	virtual void print(FILE *f) const = 0;
	virtual void dump(EvalEnv *env, FILE *f) const = 0;
};

class Line : public Object {
protected:
	std::vector<ExprAST*> list_;
public:
	Line(Pos pos, std::vector<ExprAST*> list);

	virtual void print(FILE *f) const;
	virtual void dump(EvalEnv *env, FILE *f) const;
};

class Cycle : public Line {
protected:
public:
	Cycle(Pos pos, std::vector<ExprAST*> list) :
			Line(pos, list) {}

	virtual void print(FILE *f) const;
	virtual void dump(EvalEnv *env, FILE *f) const;
};

class Rectangle : public Object {
protected:
	ExprAST *p1_, *p2_;
public:
	Rectangle(Pos pos, ExprAST *p1, ExprAST *p2);

	virtual void print(FILE *f) const;
	virtual void dump(EvalEnv *env, FILE *f) const;
};

class Circle : public Object {
protected:
	ExprAST *c_, *r_;
public:
	Circle(Pos pos, ExprAST *c, ExprAST *r);

	virtual void print(FILE *f) const;
	virtual void dump(EvalEnv *env, FILE *f) const;
};

class Arc : public Circle {
protected:
	ExprAST *st_, *ed_;
public:
	Arc(Pos pos, ExprAST *c, ExprAST *r, ExprAST *st, ExprAST *ed);

	virtual void print(FILE *f) const;
	virtual void dump(EvalEnv *env, FILE *f) const;
};

class Node : public Object {
protected:
	OptionList *opt_;
	ExprAST *loc_, *text_;
	ExprAST *deg_;
public:
	Node(Pos pos, OptionList *opt, ExprAST *loc, ExprAST *text, ExprAST *deg);

	virtual void print(FILE *f) const;
	virtual void dump(EvalEnv *env, FILE *f) const;
};

class DrawStmt : public StmtAST {
private:
	OptionList *opt_;
	Object *obj_;
public:
	DrawStmt(Pos pos, OptionList *opt, Object *obj) :
			StmtAST(pos), opt_(opt), obj_(obj) {}

	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class StmtSeq : public StmtAST {
private:
	std::vector<StmtAST*> list_;
public:
	StmtSeq(Pos pos) : StmtAST(pos) {}
	StmtSeq(Pos pos, std::vector<StmtAST*> list) :
			StmtAST(pos), list_(list) {}

	void add(StmtAST *stmt) { list_.push_back(stmt); }
	size_t size() const { return list_.size(); }
	StmtAST *get(int index) { return list_[index]; }
	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

struct Arg {
	bool def = false;
	Store* store;
	Type ty;
	Value val;

	Arg() = default;
	Arg(Store *s, Type t, Value v, bool d):
			store(s), ty(t), val(v), def(d) {}
};

class ProcDef : public AST {
private:
	Identifier *name_;
	std::vector<Arg> args_;
	int min_args_;
	std::vector<StmtAST*> stmts_;
	int vars_;
public:
	ProcDef(Identifier *name, std::vector<Arg> args);

	Identifier* name() const { return name_; }
	std::string getName() const { return name_->getName(); }
	void addStmt(StmtAST *stmt) { stmts_.push_back(stmt); }
	void setVars(int vars) { vars_ = vars; }
	int  getVars() const { return vars_; }
	int  minSize() const { return min_args_; }
	int  size() const { return args_.size(); }
	Type getArgType(int index) const { return args_[index].ty; }
	void print(FILE *f) const;
	void execute(EvalEnv *env, FILE *f) const;
	const std::vector<Arg> &getArgs() const { return args_; }
};

class FuncDef : public AST {
private:
	Identifier *name_;
	Type ty_;
	std::vector<Arg> args_;
	ExprAST *expr_;
	int vars_;
	int min_args_;
public:
	FuncDef(Identifier *name, Type ty, std::vector<Arg> args, ExprAST *expr);

	Identifier* name() const { return name_; }
	std::string getName() const { return name_->getName(); }
	void setVars(int vars) { vars_ = vars; }
	int  getVars() const { return vars_; }
	int  minSize() const { return min_args_; }
	int  size() const { return args_.size(); }
	Type getArgType(int index) const { return args_[index].ty; }
	Type type() const { return ty_; }
	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
	const std::vector<Arg> &getArgs() const { return args_; }
};

class ProcCall : public StmtAST {
private:
	Identifier *name_;
	std::vector<ExprAST*> list_;
	ProcDef *ref_;
public:
	ProcCall(Identifier *name, std::vector<ExprAST*> list) :
			StmtAST(*name), name_(name), list_(list), ref_(NULL) {}

	Identifier* name() const { return name_; }
	std::string getName() const { return name_->getName(); }
	int size() const { return list_.size(); }
	void castArg(int index, Type ty);
	void setProcDef(ProcDef *def) { ref_ = def; }
	void print(FILE *f) const;
	void exec(EvalEnv *env, FILE *f) const;
};

class FuncCall : public ExprAST {
private:
	Identifier *name_;
	std::vector<ExprAST*> list_;
	FuncDef *ref_;
public:
	FuncCall(Identifier *name, Type ty, std::vector<ExprAST*> list) :
			ExprAST(*name, ty, i_call), name_(name), list_(list), ref_(NULL) {}

	Identifier* name() const { return name_; }
	std::string getName() const { return name_->getName(); }
	int size() const { return list_.size(); }
	void castArg(int index, Type ty);
	void setFuncDef(FuncDef *def) { ref_ = def; }
	void print(FILE *f) const;
	Value eval(EvalEnv *env) const;
};

class Program : public AST {
protected:
	OptionList *opt_;
	std::vector<std::string> header_;
	std::map<std::string, ProcDef*> procs_;
	std::map<std::string, FuncDef*> funcs_;
	int vars_;
public:
	Program(Pos pos, OptionList *opt) : AST(pos), opt_(opt), vars_(0) {}

	void addHeader(std::string s) { header_.push_back(s); }
	void addProcDef(ProcDef *p);
	void addFuncDef(FuncDef *p);
	ProcDef* getProcDef(Identifier *name) const;
	FuncDef* getFuncDef(Identifier *name) const;
	void setVars(int vars) { vars_ = vars; }
	int  getVars() const { return vars_; }
	void print(FILE *f) const;
	void execute(FILE *f) const;
};

ExprAST *binary(Operator *op, ExprAST *e1, ExprAST *e2);
ExprAST *unary(Operator *op, ExprAST *e);

void debug(AST *ast, FILE *f = stderr);

#endif
