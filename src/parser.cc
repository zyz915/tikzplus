#include <cstdlib>
#include <stack>
#include <vector>
#include <map>

#include "ast.h"
#include "errorinfo.h"
#include "parser.h"
#include "tokenizer.h"
#include "types.h"
#include "eval.h"

using namespace std;

extern Token CurTok;

static StmtAST* parseStmt();
static ExprAST* parseExpr();
static ExprAST* parsePrimary();
static OptionList* parseOptionList();

static char err[128];
static TypeEnv *glb, *loc;
static vector<ProcCall*> pcalls;
static vector<FuncCall*> fcalls;

static map<string, Type> ftype;

static void setFuncType(Identifier *name, Type t) {
	if (ftype.find(name->getName()) != ftype.end()) {
		sprintf(err, "redefined function %s", name->getName().c_str());
		prtError(*name, err);
	}
	ftype.insert(make_pair(name->getName(), t));
}

static Type getFuncType(Identifier *name) {
	auto v = ftype.find(name->getName());
	if (v == ftype.end()) {
		sprintf(err, "undefined function %s", name->getName().c_str());
		prtError(*name, err);
	}
	return v->second;
}

static void setType(TypeEnv *env, Identifier *name, Type t) {
	if (env->getType(name) != t_unit) {
		sprintf(err, "redefinition of %s", name->getName().c_str());
		prtError(*name, err);
	}
	env->setType(name, t);
}

static void eatToken(int type) {
	if (CurTok.type != type) {
		if (type > tok_link)
			sprintf(err, "expect '%s' but '%s' is found",
					getTokenStr(type), getTokenStr(CurTok));
		else
			sprintf(err, "expect %s but '%s'(%d) is found",
					getTokenStr(type), getTokenStr(CurTok), CurTok.type);
		prtParseError(CurTok, err);
	}
	getNextToken();
}

static Identifier* eatNameToken() {
	if (CurTok.type != tok_name) {
		sprintf(err, "expect Identifier but '%s' is found",
				getTokenStr(CurTok));
		prtParseError(CurTok, err);
	}
	Identifier *ret = new Identifier(CurTok, string(CurTok.val));
	getNextToken();
	return ret;
}

static void eatSemicolon() {
	if (CurTok.type == tok_semicol)
		eatToken(tok_semicol);
}

static bool isBinOp(int type) {
	return (type >= tok_add  && type <= tok_shr) ||
			type == tok_ques || type == tok_colon;
}

static bool isBuildin(int type) {
	return ((type >= tok_abs && type <= tok_concat) ||
			type == tok_int || type == tok_float);
}

static bool isType(int type) {
	return (type >= tok_int && type <= tok_option);
}

static bool isStmt(int type) {
	return (type == tok_var || type == tok_anchor || type == tok_name ||
			type == tok_scale || type == tok_rotate || type == tok_if ||
			type == tok_for || type == tok_draw || isBuildin(type) ||
			type == tok_lbrace || type == tok_with || type == tok_comment);
}

static Type eatTypeToken() {
	int type = CurTok.type;
	if (type < tok_int || type > tok_option)
		prtParseError(CurTok, "invalid type");
	getNextToken();
	switch (type) {
	case tok_int:  return t_int;
	case tok_float:return t_float;
	case tok_pair: return t_pair;
	case tok_string: return t_string;
	default: return t_unit;
	}
}

static Line* parseLine() {
	Pos pos = CurTok;
	vector<ExprAST*> list;
	eatToken(tok_line);
	list.push_back(parseExpr());
	while (CurTok.type == tok_conn) {
		eatToken(tok_conn);
		list.push_back(parseExpr());
	}
	return new Line(pos, list);
}

static Cycle* parseCycle() {
	Pos pos = CurTok;
	vector<ExprAST*> list;
	eatToken(tok_cycle);
	list.push_back(parseExpr());
	while (CurTok.type == tok_conn) {
		eatToken(tok_conn);
		list.push_back(parseExpr());
	}
	return new Cycle(pos, list);
}

static Rectangle* parseRect() {
	Pos pos = CurTok;
	eatToken(tok_rect);
	ExprAST *e1 = parseExpr();
	eatToken(tok_to);
	ExprAST *e2 = parseExpr();
	return new Rectangle(pos, e1, e2);
}

static Circle* parseCircle() {
	Pos pos = CurTok;
	eatToken(tok_circle);
	ExprAST *c = parseExpr();
	eatToken(tok_radius);
	ExprAST *r = parseExpr();
	return new Circle(pos, c, r);
}

static Arc* parseArc() {
	Pos pos = CurTok;
	eatToken(tok_arc);
	ExprAST *c = parseExpr();
	eatToken(tok_radius);
	ExprAST *r = parseExpr();
	eatToken(tok_range);
	ExprAST *st = parsePrimary();
	eatToken(tok_colon);
	ExprAST *ed = parseExpr();
	return new Arc(pos, c, r, st, ed);
}

static Node* parseText() {
	Pos pos = CurTok;
	eatToken(tok_text);
	OptionList *opt = NULL;
	if (CurTok.type == tok_lbracket)
		opt = parseOptionList();
	ExprAST *deg = NULL;
	if (CurTok.type == tok_lparen) {
		eatToken(tok_lparen);
		deg = parseExpr();
		eatToken(tok_rparen);
	}
	if (deg == NULL)
		deg = new FloatExpr(Pos(), 0.0);
	ExprAST *text = parseExpr();
	if (CurTok.type == tok_at) {
		eatToken(tok_at);
		ExprAST *loc = parseExpr();
		return new Node(pos, opt, loc, text, deg);
	} else {
		Pos p; // nothing
		ExprAST *loc = new PairExpr(p, new FloatExpr(p, 0), new FloatExpr(p, 0));
		return new Node(pos, opt, loc, text, deg);
	}
}

static Node* parseNode() {
	Pos pos = CurTok;
	eatToken(tok_node);
	OptionList *opt = NULL;
	if (CurTok.type == tok_lbracket)
		opt = parseOptionList();
	eatToken(tok_at);
	ExprAST *loc = parseExpr();
	eatToken(tok_text);
	ExprAST *text = parseExpr();
	ExprAST *deg = new FloatExpr(Pos(), 0.0);
	return new Node(pos, opt, loc, text, deg);
}

static DrawStmt* parseDrawStmt() {
	Pos pos = CurTok;
	Object *obj = NULL;
	OptionList *opt = NULL;
	eatToken(tok_draw);
	if (CurTok.type == tok_lbracket)
		opt = parseOptionList();
	switch (CurTok.type) {
	case tok_line:
		obj = parseLine();
		break;
	case tok_cycle:
		obj = parseCycle();
		break;
	case tok_rect:
		obj = parseRect();
		break;
	case tok_circle:
		obj = parseCircle();
		break;
	case tok_arc:
		obj = parseArc();
		break;
	case tok_text:
		obj = parseText();
		break;
	case tok_node:
		// old interface for text
		obj = parseNode();
		break;
	default:
		prtParseError(CurTok, "unknown shape");
	}
	return new DrawStmt(pos, opt, obj);
}

static VarDef* parseBind(TypeEnv *env) {
	Identifier *name = eatNameToken();
	eatToken(tok_bind);
	/* the new version forbid this statement
	if (CurTok.type == tok_lbracket) {
		OptionList *opt = parseOptionList();
		opts->setOption(name, opt);
		return NULL;
	}
	*/
	ExprAST *expr = parseExpr();
	setType(env, name, expr->type());
	Store *lval = new Store(*name, expr->type(),
			(env != glb), env->getIndex(name));
	return new VarDef(lval, expr);
}

static StmtAST* parseVarDef(TypeEnv *env) {
	StmtSeq *seq = new StmtSeq(CurTok);
	eatToken(tok_var);
	VarDef *vd = parseBind(env);
	if (vd != NULL) seq->add(vd);
	while (CurTok.type == tok_comma) {
		eatToken(tok_comma);
		VarDef *vd = parseBind(env);
		if (vd != NULL) seq->add(vd);
	}
	if (seq->size() == 1)
		return seq->get(0);
	return seq;
}

static ExprAST* parseLetExpr() {
	StmtSeq *seq = new StmtSeq(CurTok);
	Pos pos = CurTok;
	eatToken(tok_let);
	loc->push();
	seq->add(parseBind(loc));
	while (CurTok.type == tok_comma || CurTok.type == tok_name) {
		if (CurTok.type == tok_comma)
			eatToken(tok_comma);
		seq->add(parseBind(loc));
	}
	eatToken(tok_in);
	ExprAST *ret = parseExpr();
	loc->pop();
	if (seq->size() == 0)
		return ret;
	if (seq->size() == 1)
		return new LetExpr(pos, seq->get(0), ret);
	return new LetExpr(pos, seq, ret);
}

static MultiLine* parseMultiLine() {
	Pos pos = CurTok;
	vector<ExprAST*> list;
	eatToken(tok_multi);
	string sep;
	if (CurTok.type == tok_lbracket) {
		eatToken(tok_lbracket);
		if (CurTok.type == tok_spec) {
			sep = string(CurTok.val);
			getNextToken();
		} else
			prtParseError(CurTok, "expect the value of linesep");
		eatToken(tok_rbracket);
	}
	eatToken(tok_lbrace);
	list.push_back(parseExpr());
	while (CurTok.type == tok_comma) {
		eatToken(tok_comma);
		list.push_back(parseExpr());
	}
	eatToken(tok_rbrace);
	return new MultiLine(pos, sep, list);
}

static IfStmt* parseIfStmt() {
	Pos pos = CurTok;
	eatToken(tok_if);
	eatToken(tok_lparen);
	ExprAST *cond = parseExpr();
	eatToken(tok_rparen);
	loc->push();
	StmtAST *s1 = parseStmt(), *s2 = NULL;
	loc->pop();
	if (CurTok.type == tok_else) {
		loc->push();
		eatToken(tok_else);
		s2 = parseStmt();
		loc->pop();
	}
	return new IfStmt(pos, cond, s1, s2);
}

static StmtAST* parseForStmt() {
	Pos pos = CurTok;
	eatToken(tok_for);
	Identifier *name = eatNameToken();
	if (CurTok.type == tok_bind) {
		eatToken(tok_bind);
		ExprAST *st = parseExpr();
		loc->push();
		setType(loc, name, t_int);
		Store *lval = new Store(*name, t_int,
				1, loc->getIndex(name));
		if (CurTok.type != tok_to && CurTok.type != tok_downto)
			prtParseError(CurTok, "expect 'to' or 'downto'");
		int inc = (CurTok.type == tok_to);
		getNextToken();
		ExprAST *ed = parseExpr();
		eatToken(tok_do);
		StmtAST *body = parseStmt();
		loc->pop();
		return new ForStmt(pos, lval, st, ed, inc, body);
	} else {
		vector<ExprAST*> list;
		eatToken(tok_in);
		eatToken(tok_lparen);
		loc->push();
		list.push_back(parseExpr());
		Type ty = list[0]->type();
		setType(loc, name, ty);
		Store *lval = new Store(*name, ty,
				1, loc->getIndex(name));
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			list.push_back(parseExpr());
		}
		eatToken(tok_rparen);
		eatToken(tok_do);
		StmtAST *body = parseStmt();
		loc->pop();
		return new IterStmt(pos, lval, list, body);
	}
}

static StmtAST* parseStmtSeq() {
	StmtSeq *seq = new StmtSeq(CurTok);
	loc->push();
	eatToken(tok_lbrace);
	while (isStmt(CurTok.type))
		seq->add(parseStmt());
	eatToken(tok_rbrace);
	loc->pop();
	if (seq->size() == 1)
		return seq->get(0);
	return seq;
}

static ProcCall* parseProcCall() {
	if (CurTok.type != tok_name)
		prtParseError(CurTok, "unknown function name");
	Identifier *name = eatNameToken();
	eatToken(tok_lparen);
	vector<ExprAST*> list;
	if (CurTok.type != tok_rparen) {
		list.push_back(parseExpr());
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			list.push_back(parseExpr());
		}
	}
	eatToken(tok_rparen);
	pcalls.push_back(new ProcCall(name, list));
	return pcalls.back();
}

static FuncCall* parseFuncCall(Identifier *name) {
	Type ty = getFuncType(name);
	eatToken(tok_lparen);
	vector<ExprAST*> list;
	if (CurTok.type != tok_rparen) {
		list.push_back(parseExpr());
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			list.push_back(parseExpr());
		}
	}
	eatToken(tok_rparen);
	fcalls.push_back(new FuncCall(name, ty, list));
	return fcalls.back();
}

static StmtAST* parseStmt() {
	StmtAST *ret = NULL;
	Pos pos = CurTok;
	switch (CurTok.type) {
	case tok_var:
		ret = parseVarDef(loc);
		break;
	case tok_if:
		ret = parseIfStmt();
		break;
	case tok_for:
		ret = parseForStmt();
		break;
	case tok_draw:
		ret = parseDrawStmt();
		break;
	case tok_anchor:
		eatToken(tok_anchor);
		ret = new GotoStmt(pos, parseExpr(), parseStmt());
		break;
	case tok_scale:
		eatToken(tok_scale);
		ret = new ScaleStmt(pos, parseExpr(), parseStmt());
		break;
	case tok_rotate:
		eatToken(tok_rotate);
		ret = new RotateStmt(pos, parseExpr(), parseStmt());
		break;
	case tok_with:
		eatToken(tok_with);
		ret = new WithStmt(pos, parseOptionList(), parseStmt());
		break;
	case tok_comment:
		eatToken(tok_comment);
		ret = new CommentStmt(pos, true, parseStmt());
		break;
	case tok_lbrace:
		ret = parseStmtSeq();
		break;
	case tok_name:
		ret = parseProcCall();
		break;
	default:
		prtParseError(CurTok, "no statement");
	}
	eatSemicolon();
	return ret;
}

static ExprAST* parseUnaryFunc() {
	Operator *op = new Operator(CurTok, CurTok.type);
	getNextToken();
	eatToken(tok_lparen);
	ExprAST *expr = parseExpr();
	eatToken(tok_rparen);
	return unary(op, expr);
}

static ExprAST* parseBinaryFunc() {
	Operator *op = new Operator(CurTok, CurTok.type);
	getNextToken();
	eatToken(tok_lparen);
	ExprAST *e1 = parseExpr();
	eatToken(tok_comma);
	ExprAST *e2 = parseExpr();
	eatToken(tok_rparen);
	return binary(op, e1, e2);
}

static Option* parseOption() {
	if (CurTok.type == tok_link) {
		Identifier *name = new Identifier(CurTok, string(CurTok.val + 1));
		getNextToken();
		Type t = loc->getType(name);
		if (t != t_unit)
			return new OptionRef(CurTok, new Store(CurTok, t, 1, loc->getIndex(name)));
		t = glb->getType(name);
		if (t != t_unit)
			return new OptionRef(CurTok, new Store(CurTok, t, 0, glb->getIndex(name)));
		sprintf(err, "undefined variable %s", name->getName().c_str());
		prtError(*name, err);
		return NULL;
	}
	if (CurTok.type == tok_spec) {
		OptionStr *opt = new OptionStr(CurTok, string(CurTok.val));
		getNextToken();
		return opt;
	}
	prtParseError(CurTok, "require option or @var");
	return NULL;
}

static OptionList* parseOptionList() {
	OptionList *ret = new OptionList(CurTok);
	eatToken(tok_lbracket);
	if (CurTok.type != tok_rbracket) {
		ret->add(parseOption());
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			ret->add(parseOption());
		}
	}
	eatToken(tok_rbracket);
	return ret;
}

static ExprAST* parseIdentifier() {
	Pos pos = CurTok;
	Identifier *name = eatNameToken();
	if (CurTok.type == tok_lparen)
		return parseFuncCall(name);
	Type t = loc->getType(name);
	if (t != t_unit)
		return new Store(pos, t, 1, loc->getIndex(name));
	t = glb->getType(name);
	if (t != t_unit)
		return new Store(pos, t, 0, glb->getIndex(name));
	sprintf(err, "undefined variable %s", name->getName().c_str());
	prtError(*name, err);
	return NULL;
}

static Value parseValue(Type t, int sgn) {
	// negation
	if (CurTok.type == tok_sub) {
		if (t == t_unit || t == t_string)
			prtError(CurTok, "type mismatch");
		eatToken(tok_sub);
		return parseValue(t, -1);
	}
	// parse value
	const char *val = CurTok.val;
	if (t == t_int) {
		eatToken(tok_ival);
		return ivalue(atoi(CurTok.val) * sgn);
	}
	if (t == t_float) {
		eatToken(tok_fval);
		return fvalue(atof(CurTok.val) * sgn);
	}
	if (t == t_pair) {
		eatToken(tok_lparen);
		Value v1 = parseValue(t_float, sgn);
		eatToken(tok_comma);
		Value v2 = parseValue(t_float, sgn);
		eatToken(tok_rparen);
		return pvalue(v1.f1, v2.f1);
	}
	if (t == t_string) {
		eatToken(tok_sval);
		int l = strlen(val);
		char *s = (char*) malloc(l);
		memcpy(s, val + 1, l - 2);
		return svalue(s);
	}
	return Value();
}

static Value parseDefaultValue(Type t, bool &def) {
	if (t == t_unit)
		prtParseError(CurTok, "invalid value type");
	if (CurTok.type == tok_bind) {
		def = true;
		eatToken(tok_bind);
		return parseValue(t, 1);
	} else {
		def = false;
		return Value(); // no default value
	}
}

static ExprAST* parsePrimary() {
	Pos pos = CurTok;
	ExprAST *ret = NULL;
	switch (CurTok.type) {
	case tok_ival:
		ret = new IntExpr(CurTok, atoi(CurTok.val));
		eatToken(tok_ival);
		break;
	case tok_fval:
		ret = new FloatExpr(CurTok, atof(CurTok.val));
		eatToken(tok_fval);
		break;
	case tok_sval:
		ret = new StringExpr(CurTok, string(CurTok.val + 1,
				strlen(CurTok.val) - 2));
		eatToken(tok_sval);
		break;
	case tok_true:
	case tok_false:
		ret = new IntExpr(CurTok, CurTok.type == tok_true);
		getNextToken();
		break;
	case tok_name:
		ret = parseIdentifier();
		break;
	case tok_lparen:
		eatToken(tok_lparen);
		ret = parseExpr();
		if (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			ret = new PairExpr(pos, ret, parseExpr());
		}
		eatToken(tok_rparen);
		break;
	case tok_sub:
		eatToken(tok_sub);
		ret = unary(new Operator(pos, tok_sub),
				parsePrimary());
		break;
	case tok_not:
		eatToken(tok_not);
		ret = unary(new Operator(pos, tok_not),
				parsePrimary());
		break;
	case tok_let:
		ret = parseLetExpr();
		break;
	case tok_multi:
		ret = parseMultiLine();
		break;
	case tok_abs:
	case tok_fabs:
	case tok_sin:
	case tok_cos:
	case tok_tan:
	case tok_sqrt:
	case tok_fst:
	case tok_snd:
	case tok_int:
	case tok_float:
	case tok_str:
		ret = parseUnaryFunc();
		break;
	case tok_atan2:
	case tok_dis:
	case tok_max:
	case tok_min:
	case tok_concat:
		ret = parseBinaryFunc();
		break;
	default:
		sprintf(err, "require expression but '%s' is found",
				getTokenStr(CurTok));
		prtParseError(CurTok, err);
	}
	return ret;
}

static int pre[128]; // precedence table
static void initPrecedence() {
	pre[tok_as] = 1;
	pre[tok_ques ] = 2;
	pre[tok_colon] = 3;
	pre[tok_or ] = 4;
	pre[tok_and] = 5;
	pre[tok_eq ] = 8;
	pre[tok_ne ] = 8;
	pre[tok_le ] = 9;
	pre[tok_lt ] = 9;
	pre[tok_ge ] = 9;
	pre[tok_gt ] = 9;
	pre[tok_shl] = 10;
	pre[tok_shr] = 10;
	pre[tok_add] = 11;
	pre[tok_sub] = 11;
	pre[tok_mul] = 12;
	pre[tok_div] = 12;
	pre[tok_mod] = 12;
	pre[tok_not] = 13;
}

static void reduce(stack<ExprAST*> &exps, stack<Operator*> &ops) {
	Operator *op = ops.top();
	ops.pop();
	ExprAST *e2 = exps.top();
	exps.pop();
	ExprAST *e1 = exps.top();
	exps.pop();
	if (op->type == tok_colon) {
		if (ops.empty() || ops.top()->type != tok_ques)
			prtParseError(*op, "cannot match ':' with some '?'");
		ops.pop();
		ExprAST *cond = exps.top();
		exps.pop();
		exps.push(new IfExpr(*cond, cond, e1, e2));
	} else {
		exps.push(binary(op, e1, e2));
	}
}

static ExprAST *parseExpr() {
	stack<ExprAST*> exps;
	stack<Operator*> ops;
	exps.push(parsePrimary());
	int type = CurTok.type;
	while (isBinOp(type)) {
		while (!ops.empty() && pre[type] <= pre[ops.top()->type])
			reduce(exps, ops);
		ops.push(new Operator(CurTok, type));
		getNextToken();
		exps.push(parsePrimary());
		type = CurTok.type;
	}
	while (!ops.empty())
		reduce(exps, ops);
	if (CurTok.type == tok_as) {
		Pos p = CurTok;
		eatToken(tok_as);
		ExprAST *expr = exps.top();
		Identifier *name = eatNameToken();
		setType(loc, name, expr->type());
		Store *lval = new Store(*name, expr->type(),
				1, loc->getIndex(name));
		return new AsExpr(p, lval, expr);
	}
	return exps.top();
}

static ProcDef* parseProcDef() {
	TypeEnv *tmp = loc;
	loc = new TypeEnv();
	eatToken(tok_def);
	Identifier *name = eatNameToken();
	vector<Arg> args;
	eatToken(tok_lparen);
	if (CurTok.type != tok_rparen) {
		bool def = false;
		Type t = eatTypeToken();
		Identifier *var = eatNameToken();
		Value val = parseDefaultValue(t, def);
		setType(loc, var, t);
		Store *lval = new Store(*name, t, 1, loc->getIndex(name));
		args.push_back(Arg(lval, t, val, def));
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			t = eatTypeToken();
			var = eatNameToken();
			val = parseDefaultValue(t, def);
			setType(loc, var, t);
			Store *lval = new Store(*name, t, 1, loc->getIndex(name));
			args.push_back(Arg(lval, t, val, def));
		}
	}
	eatToken(tok_rparen);
	ProcDef *ret = new ProcDef(name, args);
	ret->addStmt(parseStmtSeq());
	ret->setVars(loc->size());
	eatSemicolon();
	delete loc;
	loc = tmp;
	return ret;
}

static FuncDef* parseFuncDef() {
	TypeEnv *tmp = loc;
	loc = new TypeEnv();
	eatToken(tok_fn);
	Identifier *name = eatNameToken();
	vector<Arg> args;
	eatToken(tok_lparen);
	if (CurTok.type != tok_rparen) {
		bool def = false;
		Type t = eatTypeToken();
		Identifier *var = eatNameToken();
		Value val = parseDefaultValue(t, def);
		setType(loc, var, t);
		Store *lval = new Store(*name, t, 1, loc->getIndex(name));
		args.push_back(Arg(lval, t, val, def));
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			t = eatTypeToken();
			var = eatNameToken();
			val = parseDefaultValue(t, def);
			setType(loc, var, t);
			Store *lval = new Store(*name, t, 1, loc->getIndex(name));
			args.push_back(Arg(lval, t, val, def));
		}
	}
	eatToken(tok_rparen);
	eatToken(tok_colon);
	Type ty = eatTypeToken();
	setFuncType(name, ty);
	eatToken(tok_bind);
	FuncDef *ret = new FuncDef(name, ty, args, parseExpr());
	ret->setVars(loc->size());
	eatSemicolon();
	delete loc;
	loc = tmp;
	return ret;
}

static void parseGlobals(Program *prog) {
	glb = new TypeEnv();
	loc = new TypeEnv();
	ProcDef *proc = new ProcDef(
		new Identifier(Pos(), "_main_"), vector<Arg>());
	while (true) {
		int type = CurTok.type;
		if (type == tok_head) {
			prog->addHeader(string(CurTok.val));
			eatToken(tok_head);
		} else
		if (type == tok_def) {
			prog->addProcDef(parseProcDef());
			eatSemicolon();
		} else
		if (type == tok_fn) {
			prog->addFuncDef(parseFuncDef());
			eatSemicolon();
		} else
		if (type == tok_var) {
			proc->addStmt(parseVarDef(glb));
			eatSemicolon();
		} else
		if (isStmt(type)) {
			proc->addStmt(parseStmt());
		} else break;
	}
	proc->setVars(loc->size());
	prog->addProcDef(proc);
	prog->setVars(glb->size());
	for (auto call : pcalls) {
		ProcDef *def = prog->getProcDef(call->name());
		if (call->size() < def->minSize())
			prtError(*call, "insufficient number of parameters");
		if (call->size() > def->size())
			prtError(*call, "too many parameters");
		for (int i = 0; i < call->size(); i++)
			call->castArg(i, def->getArgType(i));
		call->setProcDef(def);
	}
	for (auto call : fcalls) {
		FuncDef *def = prog->getFuncDef(call->name());
		if (call->size() < def->minSize())
			prtError(*call, "insufficient number of parameters");
		if (call->size() > def->size())
			prtError(*call, "too many parameters");
		for (int i = 0; i < call->size(); i++)
			call->castArg(i, def->getArgType(i));
		call->setFuncDef(def);
	}
}

static Program* parseProgram() {
	Pos pos = CurTok;
	OptionList *opt = NULL;
	eatToken(tok_tikz);
	if (CurTok.type == tok_lbracket)
		opt = parseOptionList();
	Program *ret = new Program(pos, opt);
	eatToken(tok_lbrace);
	parseGlobals(ret);
	eatToken(tok_rbrace);
	return ret;
}

Program* parse() {
	initTokenizer();
	initPrecedence();
	getNextToken();
	return parseProgram();
}
