#ifndef ERRORINFO_H
#define ERRORINFO_H

struct Pos {
	int row;
	int col;

	Pos():row(0),col(0) {}
	Pos(int row, int col):row(row),col(col) {}
	Pos(const Pos &pos):row(pos.row),col(pos.col) {}
};

void prtError(const char *msg);
void prtError(Pos pos, const char *msg);
void prtWarning(Pos pos, const char *msg);

void prtTypeError(Pos pos, const char *msg);
void prtParseError(Pos pos, const char *msg);

void t_assert(bool val, const char *msg);

void setline(int line, const char *buff);

#endif
