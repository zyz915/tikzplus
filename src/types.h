#ifndef _TYPES_H
#define _TYPES_H

#include <map>
#include <vector>

class Identifier;
class OptionList;

enum Type {
	t_unit, t_int, t_float, t_pair, t_string
};

class TypeEnv {
private:
	std::vector<std::map<std::string, Type> > st_;
	std::map<std::string, int> mp_;
	int size_, mx_;
public:
	TypeEnv();

	void push();
	void pop();
	void setType(const Identifier *name, Type t);
	Type getType(const Identifier *name) const;
	int getIndex(const Identifier *name) const;
	int size();
};

const char *getTypeStr(Type t);

Type unifyType(Type t1, Type t2);

#endif

