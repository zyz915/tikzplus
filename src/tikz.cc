#include "ast.h"
#include "parser.h"
#include "tokenizer.h"

#include <cstdio>

const char* output(const char *name) {
	static char ret[64];
	int l = strlen(name), p = l - 1;
	while (p >= 0 && name[p] != '.') p--;
	if (p < 0) {
		sprintf(ret, "%s.tex", name);
		return ret;
	} else {
		memcpy(ret, name, p);
		sprintf(ret + p, ".tex");
		return ret;
	}
}

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("usage: %s <filename>\n", argv[0]);
		return 0;
	}
	setInputStream(fopen(argv[1], "r"));
	Program *prog = parse();
	FILE *fout = fopen(output(argv[1]), "w");
	prog->execute(fout);
	printf("pdflatex %s\n", output(argv[1]));
	return 0;
}
