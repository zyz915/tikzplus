#include "tokenizer.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

extern Token CurTok;

static int end = 0;
static int debug;
static char buff[1024];

static void putToken(char *buff, Token tok) {
	if (debug) {
		fprintf(stderr, "%2d (%d.%d) %s\n", tok.type,
				tok.row, tok.col, getTokenStr(tok));
		return;
	}
	while (end < tok.col)
		buff[end++] = ' ';
	int pos = tok.col - 1;
	char *dst = buff + pos;
	const char *src = getTokenStr(tok);
	int l = strlen(src);
	for (int i = 0; i < l; i++)
		dst[i] = (src[i] == '\t' ? ' ' : src[i]);
	end = pos + l;
	buff[end] = 0;
}

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("usage: %s <filename>\n", argv[0]);
		return 0;
	}
	setInputStream(fopen(argv[1], "r"));
	debug = (argc > 2 && !strcmp(argv[2], "-d"));
	int currline = 1;
	buff[end] = 0;
	initTokenizer();
 	getNextToken(true);
	while (CurTok.type != tok_eof) {
		while (CurTok.row != currline) {
			int st = 0;
			while (st < end && buff[st] != '\n') st++;
			if (!debug) {
				if (st == end) {
					printf("%s\n", buff);
					buff[end = 0] = 0;
				} else {
					st += 1;
					for (int i = 0; i < st; i++)
						putchar(buff[i]);
					for (int i = st; i < end; i++)
						buff[i - st] = buff[i];
					end -= st;
					buff[end] = 0;
				}
			}
			currline += 1;
		}
		putToken(buff, CurTok);
		getNextToken(true);
	}
	printf("%s\n", buff);
	return 0;
}
