#include "ast.h"
#include "parser.h"
#include "tokenizer.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

extern Token CurTok;

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("usage: %s <filename>\n", argv[0]);
		return 0;
	}
	setInputStream(fopen(argv[1], "r"));
	Program *prog = parse();
	prog->print(stdout);
	//prog->execute(stdout);
	return 0;
}
