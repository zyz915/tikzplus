#include <cstdio>
#include <cstdlib>
#include <map>
#include <string>

#include "errorinfo.h"

using namespace std;

static map<int, string> prog;
void setline(int line, const char *buff) {
	prog.insert(make_pair(line, string(buff)));
}

static void locate(Pos pos) {
	auto v = prog.find(pos.row);
	if (v == prog.end()) return;
	const char *s = v->second.c_str();
	int l = v->second.length();
	if (pos.col < 1 || pos.col > l) return;
	for (int i = 0; s[i] && s[i] != '\n'; i++)
		fputc(s[i], stdout);
	fputc('\n', stdout);
	for (int i = 0; i < pos.col - 1; i++)
		fputc(s[i] == '\t' ? '\t' : ' ', stdout);
	fprintf(stdout, "^\n");
	fflush(stdout);
}

void prtError(const char *str) {
	fprintf(stdout, "error: %s\n", str);
	exit(-1);
}

void prtError(Pos pos, const char *str) {
	fprintf(stdout, "line %d.%d error: %s\n", pos.row, pos.col, str);
	locate(pos);
	exit(-1);
}

void prtWarning(Pos pos, const char *str) {
	fprintf(stdout, "line %d.%d warning: %s\n", pos.row, pos.col, str);
	locate(pos);
}

void prtTypeError(Pos pos, const char *str) {
	fprintf(stdout, "line %d.%d type error: %s\n", pos.row, pos.col, str);
	locate(pos);
	exit(-1);
}

void prtParseError(Pos pos, const char *str) {
	fprintf(stdout, "line %d.%d parse error: %s\n", pos.row, pos.col, str);
	locate(pos);
	exit(-1);
}

void t_assert(bool val, const char *str) {
	if (!val) {
		fprintf(stdout, "assertion failure: %s\n", str);
		exit(-1);
	}
}
