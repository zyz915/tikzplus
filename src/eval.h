#ifndef _EVAL_H
#define _EVAL_H

#include <utility>
#include <vector>

#include "types.h"

class Value {
public:
	int i;
	double f1, f2;
	const char *s;

	Value() : i(0), f1(0), f2(0), s("") {}
	Value(int i, double f1, double f2, const char *s) :
			i(i), f1(f1), f2(f2), s(s) {}
};

inline Value ivalue(int val) { return Value(val, 0, 0, ""); }
inline Value fvalue(double val) { return Value(0, val, 0, ""); }
inline Value svalue(const char *val) { return Value(0, 0, 0, val); }
inline Value pvalue(double v1, double v2) { return Value(0, v1, v2, ""); }

struct Arg;
class ValueMap {
private:
	Value *vals_;
	int num_;

public:
	ValueMap(int num);
	ValueMap(int num, const std::vector<Arg> &args); // default values
	~ValueMap();

	Value get(int index) const;
	void set(int index, const Value &val);
};

class EvalEnv {
private:
	ValueMap *glb_, *loc_;
	double tr[6] = {0};
	std::vector<OptionList*> opts_;

public:
	EvalEnv(ValueMap *glb, ValueMap *loc) : glb_(glb), loc_(loc) {}
	~EvalEnv() {}

	Value getLoc(int index) const { return loc_->get(index); }
	Value getGlb(int index) const { return glb_->get(index); }
	void setLoc(int index, const Value &val) { loc_->set(index, val); }
	void setGlb(int index, const Value &val) { glb_->set(index, val); }
	ValueMap *getGlbMap() const { return glb_; }

	void pushOptionList(OptionList *opts);
	void popOptionList();
	void printOptionList(OptionList *opts, FILE *f);
};

class TransSystem {
private:
	double w[12] = {1, 0, 0, 0, 1, 0};
	double deg = 0.0;
	double sca = 1.0;
public:
	TransSystem() {}

	void move(double x, double y);
	void scale(double s);
	void rotate(double d);

	void prtPair(double x, double y, FILE *f);
	void prtLength(double l, FILE *f);

	double length(double l);

	double curr_degree() const;
	double curr_scale() const;
};

const double PI = 3.14159265358979323846;
const double cvt = PI / 180;

const char* getValueStr(const Value &val, Type ty);
const char* getFloatStr(double val);
void prettyFloat(double v, FILE *f);
void prettyPair(double x, double y, FILE *f);

#endif
